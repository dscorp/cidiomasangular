import { of as observableOf, Observable } from "rxjs";
import { Directive, Injectable } from "@angular/core";
import {
  AsyncValidatorFn
} from "@angular/forms";
import { AbstractControl, ValidationErrors } from "@angular/forms";
import { map } from "rxjs/operators";
import { PersonaService } from '../components/componentsgeneral/persona/persona.service';


export class asyncValidatorDNI {



  static createValidatorActualizar(personaService: PersonaService): AsyncValidatorFn {
 
    let firstDNI:string;
    let firsttime:boolean=true;
    return (control: AbstractControl): Observable<ValidationErrors> => {
    
        if(firsttime)
        {
            firsttime=false;
            firstDNI=control.value;    
        }
        
      return personaService.verificarExistDNI(control.value).pipe(
        map((result: any) => {
            // console.log(firstDNI);
          if (result.content != null && result.dni==firstDNI) {

            return null;
          }
          else if(result.content!=null && result.content.dni != firstDNI)
          {
            return {"existe":true}
          }
          else{
              return null;
          }
        })
      );
    };
  }


  static createValidatorRegistrar(personaService: PersonaService): AsyncValidatorFn {
  
    return (control: AbstractControl): Observable<ValidationErrors> => {
      return personaService.verificarExistDNI(control.value).pipe(
        map((result: any) => {
        
          if (result.content != null) {
            // console.log('hay contenido');

            return {"existe":true};
          }
          else{
            // console.log('no hay contenido');
              return null;
          }
        })
      );
    };
  }


}
