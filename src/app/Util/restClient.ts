import { Router } from '@angular/router'
import { HttpHeaders } from '@angular/common/http';
import { FormControl, AsyncValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonaService } from '../components/componentsgeneral/persona/persona.service';



export class RestClient {
  public static readonly URL_SERVER = 'http://localhost:8080/sidi';
  // public static readonly URL_SERVER = 'http://192.168.93.243:8080/api';
  // public static readonly URL_SERVER = 'http://192.168.43.253:8080/api';




  public static headerJSON = { headers: new HttpHeaders().set('Content-Type', 'application/json') };



  public static asyncValitadorExistenciasDNI(pservice: PersonaService): AsyncValidatorFn {
    return (control: AbstractControl): Observable<{ [key: string]: any } | null> => {

    
      return pservice.verificarExistDNI(control.value)
        .pipe(
          map((res: any) => {


          

            if (res.error == false) {
     

       
              if (res.content != null) {
              
              console.log('retorna true');
                return { 'existe': true };
              }
              else {
              return null;
              }

            }
          })
        );
    };

  }



  public static noWhitespaceValidator(control: FormControl) {
    if (control.value.length > 0) {
      const isWhitespace = control.value.trim().length === 0;
      const isValid = !isWhitespace;
      return isValid ? null : { 'soloespaciosenblanco': true };
    }
    else {
      return null;
    }

  }

}
