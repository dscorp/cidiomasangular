import { Component } from '@angular/core';
import { Usuario } from './model/models';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SidiAngular';

  currentUser: Usuario;

  constructor(
    private router: Router,
    private authService: AuthService
  ){
    this.authService.currentUser.subscribe(x => this.currentUser = x);
  }

  logout() {
    console.log("Salir Cuenta");
    this.authService.logout();
    this.router.navigate(['/login']);
  }
  ngOnInit(): void {

  }
}
