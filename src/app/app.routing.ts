import { LoginComponent } from "./components/login/login.component";
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { PagesModule} from './pages/pages.module';
import { PagoModule } from './pages/pagomodule/pago.module';
import { NopagefoundComponent } from './shared/nopagefound/nopagefound.component';
import { from } from 'rxjs';


const routes: Routes = [

  { path: 'login', component: LoginComponent },
  {
    path: '',
    component: PagesComponent,
    loadChildren: './pages/pages.module#PagesModule'
  },
  { path: '**', component: NopagefoundComponent }
]

export const APP_ROUTES = RouterModule.forRoot(routes);







  // {
  //   path: 'exubicacion',
  //   children: 
  //   [
  //     { path: '', component: ListarExamenUbicacionComponent },
  //     { path: 'add', component: CrearExamenUbicacionComponent },
  //     { path: ':id/postulantes' , component: AlumnosExamenUbicacionComponent},
  //     { path: ':id/puntaje' , component: PuntajeExamenUbicacionComponent},
  //     { path: ':id/asistencia' , component: AsistenciaExamenUbicacionComponent}
  //   ]
  // },
  //   {

  //     path: 'alumno',
  //     children: 
  //     [
  //       { path: '', component: ListarAlumnosComponent },
  //       { path: 'colegio', component: AlumnoColegioComponent },
  //       { path: 'colegio/:id', component: AlumnoColegioComponent },
  //       { path: 'universidad', component: AlumnoUnivComponent },
  //       { path: 'universidad/:id', component: AlumnoUnivComponent },
  //       { path: 'test', component: RegistroUniversitarioOColegialComponent },
  //     ]
  //   },

  //   {

  //     path: 'docente',
  //     children: 
  //     [
  //       { path: '', component: ListarDocenteComponent },
  //       { path: 'add', component: RegistrarDocenteComponent },
  //       { path: ':id', component: RegistrarDocenteComponent }
  //     ]
  //   },

  //   {
  //     path: 'administrativo',
  //     children: 
  //     [
  //       { path: '', component: ListarAdministrativoComponent },
  //       { path: 'add', component: RegistrarAdministrativoComponent },
  //       { path: 'update/:id', component: RegistrarAdministrativoComponent }
  //     ]
  //   },

  //   {
  //     path: 'usuario',
  //     children: 
  //     [
  //       { path: 'alumno', component: ListarUsuarioComponent },
  //       { path: 'docente', component: ListarUsuarioDocenteComponent },
  //       { path: '', component: UserComponent },
  //       { path: 'configuracion' , component: ConfiguracionUsuarioComponent }
  //     ]
  //   },
