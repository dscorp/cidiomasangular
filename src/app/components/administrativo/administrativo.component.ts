import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Buscar } from 'src/app/model/models';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { AdministrativoService } from './administrativo.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-administrativo',
  templateUrl: './administrativo.component.html',
  styleUrls: ['./administrativo.component.css']
})
export class AdministrativoComponent implements OnInit {


  @Input('parentForm')
  public parentForm:FormGroup;

  constructor() { }

  

  ngOnInit() {
  }

  

}
