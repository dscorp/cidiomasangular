import { Injectable } from '@angular/core';
import { RestClient } from 'src/app/Util/restClient';
import { HttpClient } from '@angular/common/http';
import { Buscar, administrativo } from 'src/app/model/models';

@Injectable({
  providedIn: 'root'
})
export class AdministrativoService {

public registrando:boolean = false;

path:any = RestClient.URL_SERVER + '/administrativo/';

  constructor(private http:HttpClient) { }

  listar()
{
  return this.http.get(this.path);
}

Search(buscar: Buscar){
  return this.http.post(this.path+ 'search',buscar);
}
registrar(administrativo: administrativo){
  return this.http.post(this.path, administrativo)
}

findById(idAdministrativo)
{
return this.http.get(this.path+idAdministrativo)
}


actualizar(administrativo:administrativo)
{
    return this.http.put(this.path,administrativo)
}


}
