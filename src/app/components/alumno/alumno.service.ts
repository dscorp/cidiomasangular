import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';
import { Observable } from 'rxjs';
import { Alumno, AlumnoColegio, AlumnoUniversitario } from 'src/app/model/models';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AlumnoService {

  public registrando: boolean = false;


  path: any = RestClient.URL_SERVER + '/alumno/';

  constructor(private http: HttpClient) {

  }


  getByDni(dni:string)
  {
    return this.http.get(this.path+'getByDni/'+dni);
  }

  actualizarAlumnoCol(alumnoColegio: AlumnoColegio) {
    return this.http.put(this.path + 'colegio', alumnoColegio, RestClient.headerJSON)
  }

  findAlumnoColvById(id) {
    return this.http.get(this.path + 'colegio/findbyid/' + id)
  }

  actualizarAlumnoUniv(alumnoUniversidad: AlumnoUniversitario) {
    return this.http.put(this.path + 'universidad', alumnoUniversidad, RestClient.headerJSON)
  }

  findAlumnoUnivById(id) {
    return this.http.get(this.path + 'universidad/findbyid/' + id)
  }

  listarAlumnos() {
    return this.http.get(this.path)
  }

  listarAlumnosByExamenUbicacion(id){
    return this.http.get(this.path+'byExamenUbicacion/'+id);
  }

  listsimplebyExamenUbicacion(id){
    return this.http.get(this.path+'simple/byExamenUbicacion/'+id);
  }

  registrarAlumno(alumno:any) {
    console.log(alumno);
    return this.http.post(this.path, alumno, RestClient.headerJSON)
  }

  registrarAlumnoColegio(alumno: AlumnoColegio) {
    return this.http.post(this.path + 'ralumnocolegio', alumno, RestClient.headerJSON)
  }

  registrarAlumnoUniversidad(alumno: AlumnoUniversitario) {
    return this.http.post(this.path + 'ralumnouniversidad', alumno, RestClient.headerJSON)
  }


  buscarPorNombreOrApellidoOrDni(busqueda: string) {
      return this.http.get(this.path+ 'buscarPorNombreOrApellidoOrDni/'+busqueda)
  }

  searchByNombreOrApeedioOrDNI(query: string): Observable<any> {
    const url = this.path+ 'buscarPorNombreOrApellidoOrDni/'+query;
    return this.http
      .get<any>(url)
      .pipe(
        map(res => {
          return res;
        })
      );
  }


}
