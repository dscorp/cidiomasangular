import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteAlumnoComponent } from './autocomplete-alumno.component';

describe('AutocompleteAlumnoComponent', () => {
  let component: AutocompleteAlumnoComponent;
  let fixture: ComponentFixture<AutocompleteAlumnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutocompleteAlumnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
