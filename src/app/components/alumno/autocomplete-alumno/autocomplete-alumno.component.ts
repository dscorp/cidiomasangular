import { Component, OnInit, Input } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AbstractControl, FormControl, ValidatorFn, FormGroup, Validators } from '@angular/forms';
import { map, catchError } from 'rxjs/operators';
import { AlumnoService } from '../alumno.service';


function autocompleteStringValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {

    if (control.value.persona == null) {
      return { 'invalidAutocompleteString': { value: control.value } }
    } else {
      return null;
    }
  }
}

@Component({
  selector: 'app-autocomplete-alumno',
  templateUrl: './autocomplete-alumno.component.html',
  styleUrls: ['./autocomplete-alumno.component.css']
})
export class AutocompleteAlumnoComponent implements OnInit {

  @Input()
  parentForm: FormGroup;

  public docenteList: Observable<any> = null;
  public filteredDocenteLabelOptions: Observable<string[]>;

  public autoCompleteControl = new FormControl( '', { validators: [autocompleteStringValidator(), Validators.required] })


  public validation_msgs = {
    'autoCompleteControl': [
      { type: 'invalidAutocompleteString', message: 'Alumno No Valido , Seleciona una opcion del componente' },
      { type: 'required', message: 'Debe selecionar un Alumno.' }
    ]
  }

  constructor(
    private alumnoService: AlumnoService
  ) {

    this.autoCompleteControl.valueChanges.subscribe((data: any) => {
      if (this.autoCompleteControl.invalid) {
        this.parentForm.get("idAlumno").setValue('');
      }
    })

  }

  ngOnInit() {
    // console.log('Autocomplete Alumno Init');
    // console.log(this.parentForm);
    // console.log("Alumno ID " + this.parentForm.get("idAlumno").value);
  }

  buscar(busqueda) {
    if (busqueda) {
      this.docenteList = this.lookup(busqueda);
    }
  }

  lookup(value: string): Observable<any> {
    return this.alumnoService.searchByNombreOrApeedioOrDNI(value).pipe(
      // map the item property of the github results as our return object
      map(results => {
        return results.content;
      }),
      // catch errors
      catchError(_ => {
        return of(null);
      })
    );
  }

  eventselect(idAlumno) {
    this.parentForm.get("idAlumno").setValue(idAlumno);
  }

  getOptionText(option) {
    if (option.persona == null) {
      return
    }

    return option.persona.nombre + ' ' + option.persona.apaterno + ' ' + option.persona.amaterno;
  }

}
