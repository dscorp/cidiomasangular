import { Component, OnInit, Input, ViewChild, SimpleChanges } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AuthService } from 'src/app/services/auth.service';
import { Usuario } from 'src/app/model/models';

@Component({
  selector: 'app-base-listar-alumnos',
  templateUrl: './base-listar-alumnos.component.html',
  styleUrls: ['./base-listar-alumnos.component.css']
})
export class BaseListarAlumnosComponent implements OnInit {

  @Input()
  content:any=[];
  
  displayedColumns = ['nombre', 'apaterno', 'amaterno', 'dni','tipoAlumno','condicion'];
  dataSource: MatTableDataSource<any>;
  user : Usuario;

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;
  
  
  constructor(auth: AuthService) {

      this.user = auth.currentUserValue;
      if(this.user.tipoUsuario == "administrativo"){
        this.displayedColumns.push("action");
      }
      
   }
  
  ngOnChanges(changes: SimpleChanges) {
    if (changes['content']) {
      this.dataSource = new MatTableDataSource(this.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort; 
    }
  }

  ngOnInit() {}
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
