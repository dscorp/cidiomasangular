import { Component, OnInit, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ColegioService } from './colegio.service';


@Component({
  selector: 'app-colegio',
  templateUrl: './colegio.component.html',
  styleUrls: ['./colegio.component.css']
})
export class ColegioComponent implements OnInit {

  @Input()
  public parentForm: FormGroup;

  public colegios: any[] = [];
  // @Output() public seleccionColegioEvent = new EventEmitter<any>();

  // public colegio:Colegio = new Colegio();

  // public idColegio:number;

  constructor(private colegioservice:ColegioService) { 
   this.listarColegios()
  }

  listarColegios() {
    this.colegioservice.listarColegios().subscribe((data: any) => {
      this.colegios = data.content;
    })
  }


  ngOnInit() {
    // console.log('colegios recibidos xx')
    // console.log(this.items)
 
  }

  // selectChangeHandler (event: any) {
  //   console.log('cambio')
  //   this.seleccionColegioEvent.emit(event.target.value)
  // }




}
