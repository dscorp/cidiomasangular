import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';


@Injectable({
  providedIn: 'root'
})
export class ColegioService {
  path:any = RestClient.URL_SERVER+'/colegio';
  constructor(private http:HttpClient) { }

listarColegios()
{
  return this.http.get(this.path);
}

}
