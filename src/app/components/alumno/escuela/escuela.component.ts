import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { EscuelaService } from './escuela.service';
import { FormGroup, FormControl } from '@angular/forms';
import { AlumnoService } from '../alumno.service';
import { Universidad } from 'src/app/model/models';

@Component({
  selector: 'app-escuela',
  templateUrl: './escuela.component.html',
  styleUrls: ['./escuela.component.css']
})
export class EscuelaComponent implements OnInit {

  //@Carrillo : Este componente se utiliza en matricula y editaralumnouniv

  @Input()
  formularioPadre: FormGroup;

  @Input()
  formvalue: any;

  @Input() registrando: boolean;

  universidadSeleccionada: any = '';
  universidades: any[] = [];
  facultades: any[] = [];
  escuelas: any[] = [];
  AluFirstTime: boolean = true;
  constructor(
    private escuelaSerivce: EscuelaService
    , private alumnoService: AlumnoService

  ) {

  }

  ngOnChanges(changes: SimpleChanges): void {

    this.listarUniversidades();
  }

  ngOnInit() {



  }

  listarUniversidades() {
    if (this.AluFirstTime === true && this.formularioPadre.get('facultad').get('universidad').get('idUniversidad').value != '') {

      this.escuelaSerivce.listarUniversidades().subscribe((data: any) => {
        this.universidades = data.content;
        if (this.registrando === false) {

          let idUniversidad = this.formularioPadre.get('facultad').get('universidad').get('idUniversidad').value;
          this.listarFacutladesByUniversidad2(idUniversidad);
          let idFacultad = this.formularioPadre.get('facultad').get('idFacultad').value;
          this.listarEscuelasByFacultad2(idFacultad);
        }
        this.AluFirstTime = false;
      })
    }
    else {
      this.escuelaSerivce.listarUniversidades().subscribe((data: any) => {
        this.universidades = data.content;
      });
    }
  }

  resetearcontrolesFacultadYEscuela() {
    this.formularioPadre.get('facultad').get('idFacultad').setValue("")
    this.formularioPadre.get('idEscuela').setValue("")
  }

  resetearControlesEscuela() {
    this.formularioPadre.get('idEscuela').setValue("")
  }

  listarFacutladesByUniversidad(e: any) {



    if (e.target.value != '') {

      let universidadSeleccionada = this.universidades.find(o => o.idUniversidad + '' === e.target.value);

      this.formularioPadre.get('facultad').get('universidad').get('idUniversidad').setValue(universidadSeleccionada.idUniversidad)
      this.formularioPadre.get('facultad').get('universidad').get('nombre').setValue(universidadSeleccionada.nombre)
      this.formularioPadre.get('facultad').get('universidad').get('siglas').setValue(universidadSeleccionada.siglas)
      this.escuelaSerivce.FacutladesByUniversidad(e.target.value).subscribe((data: any) => {
        this.escuelas = null;
        this.facultades = data.content;
        this.resetearcontrolesFacultadYEscuela();
      })
    }
    else {
      // this.formularioPadre.get('facultad').get('universidad').get('idUniversidad').setValue('')
      // this.formularioPadre.get('facultad').get('universidad').get('nombre').setValue('')
      // this.formularioPadre.get('facultad').get('universidad').get('siglas').setValue('')
      this.resetearcontrolesFacultadYEscuela()
      this.facultades = null;
      this.escuelas = null;
    }
  }

  listarEscuelasByFacultad(e) {
    if (e.target.value != "") {
      this.escuelaSerivce.escuelasByFacutlad(e.target.value).subscribe((data: any) => {
        this.escuelas = data.content;
        this.resetearControlesEscuela()
      })
    }
    else {
      this.resetearControlesEscuela()
      this.escuelas = null;
    }
  }



  listarFacutladesByUniversidad2(e) {
    this.escuelaSerivce.FacutladesByUniversidad(e).subscribe((data: any) => {
      this.facultades = data.content;
    })

  }

  listarEscuelasByFacultad2(e) {
    this.escuelaSerivce.escuelasByFacutlad(e).subscribe((data: any) => {
      this.escuelas = data.content;
    })
  }

}
