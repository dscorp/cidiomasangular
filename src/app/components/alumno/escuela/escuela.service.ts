import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';

@Injectable({
  providedIn: 'root'
})
export class EscuelaService {
  
  path:string = RestClient.URL_SERVER+'/universidad/';
  pathFacultad:string = RestClient.URL_SERVER+'/facultad/';
  pathEscuela:string = RestClient.URL_SERVER+'/escuela/';
  constructor(private http:HttpClient) {

   }

  listarUniversidades()
  {
    return this.http.get(this.path);
  }

  FacutladesByUniversidad(idUniversidad)
  { 
    return this.http.get(this.pathFacultad+'universidad/'+idUniversidad)
  }

escuelasByFacutlad(idFacultad)
{
  return this.http.get(this.pathEscuela+'facultad/'+idFacultad)
}

}
