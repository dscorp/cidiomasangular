import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-alumno-colegio',
  templateUrl: './form-alumno-colegio.component.html',
  styleUrls: ['./form-alumno-colegio.component.css']
})
export class FormAlumnoColegioComponent implements OnInit {

  @Input()
  alumnoColegio:FormGroup;

  grados: any[] = [];

  constructor() { }

  ngOnInit() {
  }

  gradosPrimarios = [1, 2, 3, 4, 5, 6];
  gradosSecundarios = [1, 2, 3, 4, 5];
  listargrados(e) {

    //agregar  linea que  setee valor por defecto
    this.alumnoColegio.get('grado').reset();

    switch (e.target.value) {
      case "":
        this.grados = null;
        break;
        case "primario":
          this.grados = this.gradosPrimarios
          break;
          case "secundario":
            this.grados = this.gradosSecundarios
            break;
          }


        }

}
