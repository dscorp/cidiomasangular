import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-alumno-universitario',
  templateUrl: './form-alumno-universitario.component.html',
  styleUrls: ['./form-alumno-universitario.component.css']
})
export class FormAlumnoUniversitarioComponent implements OnInit {

  constructor() { }


  @Input()
  public alumnoUniversitario: FormGroup;

  ngOnInit() {
  }

}
