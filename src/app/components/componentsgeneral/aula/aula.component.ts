import { Component, OnInit, Input } from '@angular/core';
import { ExamenubicacionService } from '../../../components/examen-ubicacion/examenubicacion.service';
import { AulaService } from './aula.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-aula',
  templateUrl: './aula.component.html',
  styleUrls: ['./aula.component.css']
})
export class AulaComponent implements OnInit {

  pisos: string[] = [];

  aulas: any[] = [];

@Input()
parentform:FormGroup;

  constructor(
    private aulaService: AulaService
  ) {
    this.listarPisosPorAulasActivas()
  }

  ngOnInit() {
  }



  listarPisosPorAulasActivas() {
    this.aulaService.listarPisosPorAulasActivas().subscribe((data: any) => {
      this.pisos = data.content;
    })
  }

  listarAulasPorPiso($event) {
    if ($event.target.value != '') {
      let numeroPiso = $event.target.value;
      this.aulaService.listarAulasActivasPorPiso(numeroPiso).subscribe((data: any) => {
        this.aulas = data.content;
      })
    }
    else{
     this.limpiarComboAulas();
    }

  }


  limpiarComboAulas() {
    this.aulas = null;
    this.parentform.get('idAula').setValue("")
  }


}
