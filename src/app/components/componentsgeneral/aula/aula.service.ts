import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';

@Injectable({
  providedIn: 'root'
})
export class AulaService {

path=RestClient.URL_SERVER+'/aula'

  constructor(
private http:HttpClient

  ) { }


    listarPisosPorAulasActivas()
    {
      return this.http.get(this.path+'/pisos')
    }


    listarAulasActivasPorPiso(piso:string)
    {
      return this.http.get(this.path+'/aulasbypiso/'+piso);
    }

}
