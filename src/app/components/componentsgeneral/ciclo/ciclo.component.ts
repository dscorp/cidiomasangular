
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-ciclo',
  template:`
  <div [formGroup]="formCiclos">
    <select class="form-control" (change)="onselect($event)" formControlName="idCiclo" >
      <option value="">Seleccione un Ciclo</option>
      <option *ngFor="let c of inCiclos" value="{{c.idCiclo}}">
      {{c.nombre}}
      </option>
    </select>

    <div class=" text-danger" *ngIf="formCiclos.get('idCiclo').errors?.required">
      Debe seleccionar un ciclo
    </div>
</div>
  `
})
export class CicloComponent implements OnInit {


  

  @Input()
  inCiclos:any[]=[]


  @Output()
  eventEmmiter= new EventEmitter<String>();


  @Input()
  formCiclos:FormGroup;


  constructor() { 

  }


  onselect($event) {

    this.eventEmmiter.emit($event.target.value);  

  }

  ngOnInit() {
  }

}
