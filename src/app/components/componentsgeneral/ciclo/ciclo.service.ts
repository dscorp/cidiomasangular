import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';

@Injectable({
  providedIn: 'root'
})
export class CicloService {

  path: any = RestClient.URL_SERVER + '/ciclo/';
  constructor(private http: HttpClient) {

  }


  getByIdiomaAndNivel(idIdioma:string, idNivel:string)
  {
    return this.http.get(this.path+'getByNivelAndIdioma/'+idIdioma+'/'+idNivel);
  }


  listarCiclos()
  {
    return this.http.get(this.path);
  }
}
