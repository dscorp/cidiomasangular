import { Component, OnInit, Input, SimpleChanges, AfterViewInit } from '@angular/core';
import { GeoubicacionService } from './geoubicacion.service';
import { FormGroup } from '@angular/forms';
import { AlumnoService } from '../../../components/alumno/alumno.service';

@Component({
  selector: 'app-geoubicacion',
  templateUrl: './geoubicacion.component.html',
  styleUrls: ['./geoubicacion.component.css']
})
export class GeoubicacionComponent implements OnInit,AfterViewInit {

  @Input()
  formularioPadre: FormGroup;


  @Input()
  formularioPadreValue:any;

  public departamentos: any[] = [];
  public provincias: any[] = [];
  public distritos: any[] = [];
  public registrando: any;

  public firstTime: boolean = true;


  constructor(
    private geoubicacionService: GeoubicacionService
    , private alumnoService: AlumnoService

  ) {
    this.listarDepartamentos();

    if (alumnoService.registrando == false) {

      //  this.listarProvinciaUnDepartamento2(this.distrito.idDistrito)
      // //  this.geoFirstTime=false;

      // //  this.listarDistritosDeUnaProvincia2(this.distrito.provincia.idProvincia)
    }
  }


  limpiarProvinciayDistrito() {
    this.provincias = null;
    this.distritos = null;
    this.formularioPadre.get('provincia').get('idProvincia').setValue("")
    this.formularioPadre.get('idDistrito').setValue("")
  }

  limpiarDistrito() {

    this.formularioPadre.get('idDistrito').setValue("")
  }

  listarDistritosDeUnaProvincia(e) {

    if (e.target.value != "") {
      // this.limpiarDistrito()
      this.geoubicacionService.listarDistritoxProvincia(e.target.value).subscribe((data: any) => {
        // console.log(data.content)
        this.distritos = data.content;

      })
    }
    else {
      // console.log('selecciono  vacio');
      this.limpiarDistrito()
      this.distritos = null;
    }

  }


  listarProvinciaUnDepartamento(e) {
    if (e.target.value != "") {
      this.limpiarProvinciayDistrito()
      this.geoubicacionService.listarProvinciasxDepartamento(e.target.value).subscribe((data: any) => {
        // console.log(data.content)
        this.provincias = data.content;
      })
    } else {
      this.limpiarProvinciayDistrito()
    }
  }


  listarDepartamentos() {
    this.geoubicacionService.listarDepartamentos().subscribe((data: any) => {
      this.departamentos = data.content;

    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.alumnoService.registrando == false && this.formularioPadre.get('idDistrito').value != "" && this.firstTime) {
        this.firstTime=false;
      // console.log(' se esta actualiznado y idDistrito es diferente de null');
      // console.log(this.formularioPadre.get('idDistrito').value);
      // console.log(this.formularioPadre);
      this.listarProvinciaUnDepartamento2(this.formularioPadre.get('provincia').get('departamento').get('idDepartamento').value)
      // this.geoFirstTime=false;
     
      this.listarDistritosDeUnaProvincia2(this.formularioPadre.get('provincia').get('idProvincia').value)

    }

  }





  listarProvinciaUnDepartamento2(e) {
      //console.log('GEO - CARGAR PROVINCIAS');

    this.geoubicacionService.listarProvinciasxDepartamento(e).subscribe((data: any) => {
      // console.log(data.content)
      this.provincias = data.content;
    })

  }

  listarDistritosDeUnaProvincia2(e) {


    this.geoubicacionService.listarDistritoxProvincia(e).subscribe((data: any) => {
      // console.log(data.content)
      this.distritos = data.content;

    })

  }


  ngOnInit() {


  }

  ngAfterViewInit(): void {
  }
}
