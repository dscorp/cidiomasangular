import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';

@Injectable({
  providedIn: 'root'
})
export class GeoubicacionService {

  paht:any = RestClient.URL_SERVER+'/geoubicacion';

  constructor(private http:HttpClient) { }

  listarDepartamentos(){
    return this.http.get(this.paht+'/departamentos')
  }

  listarProvinciasxDepartamento(idDepartamento:any)
  {
    return this.http.get(this.paht+'/lprovinciasxdepartamento/'+idDepartamento)
  }

  listarDistritoxProvincia(idProvincia:any)
  {
    return this.http.get(this.paht+'/ldistritos/'+idProvincia)
  }
  
}
