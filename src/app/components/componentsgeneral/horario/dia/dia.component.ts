import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-dia',
  template: `
    <div [formGroup]="formDias">
      <select class="form-control" (change)="onselect($event)" formControlName="dia">
      <option value="lunes">lunes</option>
      <option value="martes">martes</option>
      <option value="miercoles">miercoles</option>
      <option value="jueves">jueves</option>
      <option value="viernes">viernes</option>
      <option value="sabado">sabado</option>
      <option value="domingo">domingo</option>
        </select>

        <div class=" text-danger" *ngIf="formDias.get('dia').errors?.required">
      Debe seleccionar un dia
    </div>
    </div>
  `
})
export class DiaComponent implements OnInit {

  @Input()
  inDias:any[]=[]

  @Output()
  eventEmmiter = new EventEmitter<any>();

  formDias: FormGroup;

  constructor() { 
    this.formDias= new FormGroup({
      dia: new FormControl('',Validators.required)
    })
  }

  onselect($event){
    this.eventEmmiter.emit($event.target.value);
  }

  ngOnInit() {
  }

}
