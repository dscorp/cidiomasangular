import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { HorarioService } from './horario.service';
import Swal from 'sweetalert2';
import { DiaService } from './dia/dia.service';


@Component({
  selector: 'app-horario',
  template:`

  <div class="form-group" (ngSubmit)="onSubmit()">

  <div class="col">
           <label>Dias</label>
           <app-dia [inDias]="dias" ></app-dia>
       </div>

    <div class="col">
      <div [formGroup]="formHorario">
        <label>Inicio</label>
          <input class="form-control" name="inicio" type="time">
      </div>
    </div>

    <div class="col">
      <div [formGroup]="formHorario">
        <label>Fin</label>
          <input class="form-control" name="fin" type="time">
      </div>
    </div>

    <button mat-button type="submit">Agregar</button>
<!-- 
    <pre>
        {{formHorario.value | json}}
    </pre> -->

  </div>
  <!-- <form [formGroup]="registrationForm" (ngSubmit)="onSubmit()">
   Add nested items dynamically
   <div class="group-gap" formArrayName="addDynamicElement">
      <h5 class="mb-3">Add Products</h5>
      
       <div class="mb-3">
       <div class="col">
           <label>Dias</label>
           <app-dia [inDias]="dias" ></app-dia>
       </div>

       <div class="col">
      <div [formGroup]="formHorario">
        <label>Inicio</label>
          <input class="form-control" name="inicio" type="time">
      </div>
    </div>
         <button type="button" class="btn btn-sm btn-success mb-3 btn-block" (click)="addItems()">Add Items</button>
         <ul class="subjectList">
            <li *ngFor="let item of addDynamicElement.controls; let i = index">
               <input type="text" class="form-control" [formControlName]="i">
            </li>
         </ul>
      </div>

      <button type="submit" class="btn btn-danger btn-lg btn-block">Submit Form</button>
   </div>
</form> -->
      `

})
export class HorarioComponent implements OnInit {

  @Input()
  inHorario:any[]=[]

  @Output()
  eventEmmiter= new EventEmitter<any>();

  formHorario: FormGroup; 

  dias: any[] = []
  diasFromEmmiter: any ='';


  constructor(
    private diaservice: DiaService, 
     private fb: FormBuilder

  ) { 

    this.formHorario = this.fb.group({
      
      // dia: new FormControl('',Validators.required),
      // incio: new FormControl('',Validators.required),
      // fin: new FormControl('',Validators.required),
      dia:['',Validators.required],
      incio: ['',Validators.required],
      fin: ['',Validators.required]

     
    })
  }

  // registrationForm = this.fb.group({
  //   addDynamicElement: this.fb.array([])
  // })

  // get addDynamicElement() {
  //   return this.registrationForm.get('addDynamicElement') as FormArray
  // }

  // addItems() {
  //   this.addDynamicElement.push(this.fb.control(''))
  // }

  onSubmit() {
    var inicio = this.formHorario.controls['inicio'].value;
  }

  onselect($event){
    this.eventEmmiter.emit($event.target.value);
  }

  onDiaSelect(dia){
    this.diasFromEmmiter = dia;
  }

  ngOnInit() {
  }


}
