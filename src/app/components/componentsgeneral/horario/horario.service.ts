import { Injectable } from '@angular/core';
import { RestClient } from 'src/app/Util/restClient';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HorarioService {

path:any = RestClient.URL_SERVER + '/horario/';

  constructor(private http:HttpClient) { }

  listar(){
    return this.http.get(this.path);
  }

  save(horario:any){
    return this.http.post(this.path,horario); 
  }
}
