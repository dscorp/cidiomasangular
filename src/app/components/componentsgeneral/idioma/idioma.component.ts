import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IdiomaService } from './idioma.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-idioma',
  template: `
  <div [formGroup]="formGroupIdioma">
     <select class="form-control" formControlName="idIdioma"  (change)="onselect($event)">
        <option value=""  >Seleccione un Idioma</option>
        <option *ngFor="let idioma of idiomas" value="{{idioma.id}}">
          {{idioma.nombre}}
        </option>
      </select>
      <div class="text-danger" *ngIf="formGroupIdioma.controls['idIdioma'].errors?.required">
          Debe seleccionar una idioma
        </div>
</div>
  `
})
export class IdiomaComponent implements OnInit {

  @Input()
  idiomas: any[] = []

  

  @Input()
  formGroupIdioma: FormGroup;
  
  
  
  constructor() {

  }
  


  @Output()
  emitterIdioma = new EventEmitter<String>();

  onselect($event) {
    this.emitterIdioma.emit($event.target.value);
  }
  
  ngOnInit() {
  
}


}
