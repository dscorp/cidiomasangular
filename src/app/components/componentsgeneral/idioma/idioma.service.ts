import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from '../../../Util/restClient';

@Injectable({
  providedIn: 'root'
})
export class IdiomaService {

  path = RestClient.URL_SERVER+'/idioma/'

  constructor(private http:HttpClient) { }


  listarIdiomasActivos()
  {
    return this.http.get(this.path+'byactivo');
  }

}
