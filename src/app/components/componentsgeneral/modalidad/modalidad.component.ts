import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  selector: 'app-modalidad',
  template: `
    <div [formGroup]="formModalidad">
     <select class="form-control" formControlName="idModalidad"  (change)="onselect($event)">
        <option value=""  >Seleccione una Modalidad</option>
        <option *ngFor="let m of inModalidades" value="{{m.idModalidad}}">
          {{m.nombre}}
        </option>
      </select>
      
      <div class="text-danger" *ngIf="formModalidad.controls['idModalidad'].errors?.required">
          Debe seleccionar una modalidad
        </div>
</div>
  `
})
export class ModalidadComponent implements OnInit {

  @Input()
  formModalidad: FormGroup;

  @Input()
  inModalidades: any[] = [];

  @Output()
  emmiterModalida = new EventEmitter<any>();

  constructor() {
 
  }

  ngOnInit() {
  }

  onselect($event) {
    this.emmiterModalida.emit($event.target.value);
// =======
// import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
// import { FormGroup } from '@angular/forms';
// import { ModalidadService } from './modalidad.service';

// @Component({
//   selector: 'app-modalidad',
//   templateUrl: './modalidad.component.html',
//   styleUrls: ['./modalidad.component.css']
// })
// export class ModalidadComponent implements OnInit {


// @Output()
// emmiterModalidad = new EventEmitter<any>();

// @Input()
// formGroupModalidad: FormGroup;

// modalidades: any[] = []


// constructor(private modalidadService: ModalidadService) {

// }

// listarModalides(){
//   this.modalidadService.listarModalidades().subscribe((data: any)=>{
//     this.modalidades = data.content;
//   })
// }

// onselect($event){
//   console.log('evento seleccion modalidad');
//   console.log($event.target.value);
//   this.emmiterModalidad.emit($event.target.value);
// }

//   ngOnInit() {

  }

}
