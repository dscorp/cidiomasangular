import { Injectable } from '@angular/core';
import { RestClient } from 'src/app/Util/restClient';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ModalidadService {
  path:any = RestClient.URL_SERVER + '/modalidad/';

  constructor(private http:HttpClient) { }

  getAll()
  {
     return this.http.get(this.path);
     
  }

  listarModalidades(){
    return this.http.get(this.path+'listar');
  }
}
