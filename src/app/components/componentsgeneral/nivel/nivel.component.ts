import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { NivelService } from './nivel.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-nivel',
  template: `

<div [formGroup]="formGroupNivel">
    <select class="form-control"   formControlName="idNivel" (change)="onselect($event)" >
      <option value=""  selected >Seleccione un Nivel</option>
      <option *ngFor="let nivel of inNiveles" value="{{nivel.idNivel}}">
        {{nivel.nombre}}
      </option>
    </select>

    <div class="text-danger" *ngIf="formGroupNivel.get('idNivel').errors?.required">
          Debe seleccionar una nivel
        </div>

</div>
  `
})
export class NivelComponent implements OnInit {


  @Output()
  emmiterNivel = new EventEmitter<String>();

  @Input()
  formGroupNivel: FormGroup;

  @Input()
  inNiveles:any[]=[]

  niveles: any[] = []

 

  // nivelSeleccionado: string = '';

  constructor() {
  }

  onselect($event) {
    this.emmiterNivel.emit($event.target.value);
  }


  ngOnInit() {
  }

}
