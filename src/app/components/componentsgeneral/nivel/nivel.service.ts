import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from '../../../Util/restClient';


@Injectable({
  providedIn: 'root'
})
export class NivelService {

  path = RestClient.URL_SERVER+'/nivel/'

  constructor(private http:HttpClient) { }



  listarNiveles()
  {
    return this.http.get(this.path+'listar');
  }
}
