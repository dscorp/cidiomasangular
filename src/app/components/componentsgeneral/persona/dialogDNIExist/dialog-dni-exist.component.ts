import { Component, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup} from '@angular/forms';





@Component({
  selector: 'dialog-dni-exist',
  templateUrl: 'dialog-dni-exist.component.html',
  styleUrls: ['dialog-dni-exist.component.css']
})
export class DialogDNiExistComponent {
  hide = true;
formPassword:FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogDNiExistComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    
    ) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {

  }


confirmacion()
{

  this.data.dialogConfirmation=true;
  this.dialogRef.close();

}


}
export interface DialogData {
  dialogConfirmation: boolean;
}

