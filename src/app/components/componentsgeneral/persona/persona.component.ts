import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PersonaService } from './persona.service';
import { MatDialog } from '@angular/material';
import { DialogDNiExistComponent } from './dialogDNIExist/dialog-dni-exist.component';
import Swal from 'sweetalert2';
import { Persona } from 'src/app/model/models';
import { AlumnoService } from 'src/app/components/alumno/alumno.service';




@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css'],
})
export class PersonaComponent implements OnInit {

  dialogConfirmation: boolean = false;
  isFileChosen: boolean = false;
  fileName: string = '';

  editable: boolean = true;

  @Input('parentForm') public parentForm: FormGroup;

  @Output() alumnoPersonaIfExisteEmmiter = new EventEmitter<any>();

  constructor(
    private personaService: PersonaService,
    private alumnoService: AlumnoService,
    public dialog: MatDialog
  ) {

  }


  preUpload(event) {
    let file = event.target.files[0];
    if (event.target.files.length > 0) {
      this.isFileChosen = true;
    }
    this.fileName = file.name;
  }
  ngOnInit() {

    this.parentForm.get('dni').statusChanges.subscribe(data => {

//@carrillo 
//elimina las comas o puntos no funciona
      // let dni:String=new String(data);
      // console.log(dni.charAt(dni.length-1));
      // if(dni.charAt(dni.length)===',' || dni.charAt(dni.length)==='.' )
      // {
      //     dni = dni.substr(0,dni.length-1);
      //     this.parentForm.get('dni').setValue(dni);
      // }
      
      
      // if(dni.length>8){
      //   dni=dni.substr(0,8);
      //   this.parentForm.get('dni').setValue(dni);
      // }

//elimina los numeros a partir de octavo caracter
      if (this.parentForm.get('dni').errors != undefined && this.parentForm.get('dni').errors['existe'] && !this.parentForm.disabled) {
        this.openDialog();
      }
      else{

      }

    })
  }

  openDialog(): void {
  
    const dialogRef = this.dialog.open(DialogDNiExistComponent, {
      width: '500px',
      data: { dialogConfirmation: this.dialogConfirmation }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.personaService.cargarDatosPersonaPorDNI(this.parentForm.get('dni').value).subscribe((data: any) => {
          this.alumnoService.getByDni(this.parentForm.get('dni').value).subscribe((data: any) => {
            this.alumnoPersonaIfExisteEmmiter.emit(data.content);
          })
          this.parentForm.patchValue(data.content);
          this.parentForm.disable();

        })




      }
    });


  }



}
