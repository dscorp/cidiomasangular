import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';




@Injectable({
  providedIn: 'root'
})
export class PersonaService {


  path = RestClient.URL_SERVER+'/persona'

  constructor(private http:HttpClient) { }



  verificarExistDNI(dni)
  {
    return this.http.get(this.path+'/validardni/'+dni)
  }


  cargarDatosPersonaPorDNI(dni:any)
  {
  
    return this.http.get(this.path+'/'+dni)
  }


}
