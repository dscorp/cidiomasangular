import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';

import { Observable, of } from 'rxjs';
import { AbstractControl , FormControl, ValidatorFn, FormGroup , Validators } from '@angular/forms';
import { startWith, debounceTime, switchMap, map, catchError } from 'rxjs/operators';
import { DocenteService } from '../docente.service';
import { MatAutocomplete } from '@angular/material';

function autocompleteStringValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    
    if(control.value.persona == null){
      return { 'invalidAutocompleteString': { value: control.value } }
    }else{
      return null;
    }
  }
}

@Component({
  selector: 'app-autocomplete-docente',
  templateUrl: './autocomplete-docente.component.html',
  styleUrls: ['./autocomplete-docente.component.css']
})
export class AutocompleteDocenteComponent implements OnInit {

  @Input()
  parentForm:FormGroup;

  // @Output()
  // outDocente = new EventEmitter<any>();

  public docenteList: Observable<any> = null;
  public filteredDocenteLabelOptions: Observable<string[]>;
  public autoCompleteControl = new FormControl('', 
  { validators: [autocompleteStringValidator(), Validators.required] })

  
  public validation_msgs = {
    'autoCompleteControl': [
      { type: 'invalidAutocompleteString', message: 'Docente No Valido , Seleciona una opcion del componente' },
      { type: 'required', message: 'Debe selecionar un docente.' }
    ]
  }


  constructor
    (
      private docenteService: DocenteService,
  ) {

    this.autoCompleteControl.valueChanges.subscribe((data:any)=>{
      if(this.autoCompleteControl.invalid)
      {
        this.parentForm.get("idDocente").setValue('');
      }
    })

  }

  ngOnInit() {
    console.log('Autocomplete Docente Init');
    console.log(this.parentForm);
    console.log("DOCENTE ID " + this.parentForm.get("idDocente").value);
  }


  buscar(busqueda){
    if(busqueda){
      this.docenteList=this.lookup(busqueda);
    }
  }

  lookup(value: string): Observable<any> {
    return this.docenteService.searchByNombreOrApeedioOrDNI(value).pipe(
      // map the item property of the github results as our return object
      map(results => {
        return results.content;
      }),
      // catch errors
      catchError(_ => {
        return of(null);
      })
    );
  }

  // esto devuelve el alumno seleccionado mediante un evento el cual debe ser configurado en el componete padre
  // displayFn($event) {
  //   this.parentForm.get('idDocente').setValue($event.option.value)
  // }

  eventselect(idDocente){
    this.parentForm.get("idDocente").setValue(idDocente);
  }

  getOptionText(option) {
    if(option.persona == null){
      return
    }

    return option.persona.nombre+' '+option.persona.apaterno+' '+option.persona.amaterno;
  }

}
