import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DocenteService } from '../docente.service';

@Component({
  selector: 'app-combo-docente',
  templateUrl: './combo-docente.component.html',
  styleUrls: ['./combo-docente.component.css']
})
export class ComboDocenteComponent implements OnInit {

  @Output()
    emmiterDocente = new EventEmitter<any>();
  

  @Input()
    formGroupDocente: FormGroup;

    docentes: any[] = []

  constructor(private docenteService: DocenteService) { 
    this.listarDocentes();
  }

  listarDocentes(){
    this.docenteService.listar().subscribe((data: any)=>{
      this.docentes = data.content;
      console.log('docentes ');
      console.log(data.content);
    })
  }

  onselect($event){
    console.log('evento seleccion docente');
    console.log($event.target.value);
    this.emmiterDocente.emit($event.target.value);
  }

  ngOnInit() {
  }

}
