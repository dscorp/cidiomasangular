import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';
import { Observable } from 'rxjs';
import { Docente, Buscar } from 'src/app/model/models';
import { map } from 'rxjs/operators';
 
@Injectable({
  providedIn: 'root'
})

export class DocenteService {





  public registrando:boolean=false;
  
  
  path:any = RestClient.URL_SERVER + '/docente/';
  
  constructor(private http:HttpClient) { }

  searchByNombreOrApeedioOrDNI(query: string): Observable<any> {
    const url = this.path+ 'buscarPorNombreOrApellidoOrDni/'+query;
    return this.http
      .get<any>(url)
      .pipe(
        map(res => {
          return res;
        })
      );
  }
  


  listar(){
    return this.http.get(this.path);
  }

  search(buscar: Buscar){
    return this.http.post(this.path+ 'search',buscar);
  }

  registar(docente: Docente){
    return this.http.post(this.path, docente);
  }

  actualizar(docente: Docente){
    return this.http.put(this.path, docente);
  }

  findById(id){
     return this.http.get(this.path+id);
  }

}
