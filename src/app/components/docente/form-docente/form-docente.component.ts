import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
 
@Component({
  selector: 'app-docente',
  templateUrl: './form-docente.component.html',
  styleUrls: ['./form-docente.component.css']
})
export class FormDocenteComponent implements OnInit {

  @Input('parentForm')
  public parentForm:FormGroup;

  constructor() { }

  ngOnInit() {}

}
