import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from '../../Util/restClient';
import { AuthService } from 'src/app/services/auth.service';
import { Postulacion } from 'src/app/model/models';


@Injectable({
  providedIn: 'root'
})
export class ExamenubicacionService {


  path = RestClient.URL_SERVER+'/examenUbicacion/'

  constructor(
    private http:HttpClient,
    private auth: AuthService
  ) { }


obtenersiguientenumero(){
  return this.http.get(this.path+'siguientenumero');
}

findByNivelAndIdioma(nivel:any, idioma:any)
{
  return this.http.get(this.path+'getByNivelAndIdioma/'+nivel+'/'+idioma)
}

save(examenUbicacion:any)
{
  return this.http.post(this.path,examenUbicacion);
}

list(){
  if(this.auth.currentUserValue.tipoUsuario=='administrativo'){
    return this.http.get(this.path);
  }else{
    return this.http.get(this.path+'byDocente/3');
  }
}

saveAsistencia(listPostulaciones : Array<Postulacion>){
  return this.http.post(this.path+'asistencia', listPostulaciones);
}

savePuntaje(listPostulaciones : Array<Postulacion>){
  return this.http.post(this.path+'puntaje', listPostulaciones);
}

alumnos(id: number){
  return this.http.get(this.path+id+'/alumnos');
}

get(id : number){
  return this.http.get(this.path+id);
}

getAsistencia(id: number){
  return this.http.get(this.path+id+'/asistencia');
}

getPuntaje(id: number){
  return this.http.get(this.path+id+'/puntaje');
}

}
