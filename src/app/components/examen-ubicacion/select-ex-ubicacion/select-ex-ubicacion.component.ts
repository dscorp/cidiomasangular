import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { ExamenubicacionService } from '../examenubicacion.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { NivelService } from '../../componentsgeneral/nivel/nivel.service';
import { IdiomaService } from '../../componentsgeneral/idioma/idioma.service';


@Component({
  selector: 'app-select-ex-ubicacion',
  templateUrl: './select-ex-ubicacion.component.html',
  styleUrls: ['./select-ex-ubicacion.component.css']
})
export class SelectExUbicacionComponent implements OnInit {

  @Input()
  parentForm:FormGroup;

  formgroupHelper:FormGroup;

  examenes:any[]=[];

  niveles:any[]=[]
  idiomas: any[]=[];
  constructor(
    private examenUbicacionservice: ExamenubicacionService
,   private fbuilder:FormBuilder,
    private nivelService:NivelService,
    private IdiomaService:IdiomaService
  ) { 

this.formgroupHelper=fbuilder.group({
'idNivel':['',Validators.required],
'idIdioma':['',Validators.required]
})

  }



  idioma: any = '';
  nivel: any = '';
  ngOnInit() {
    this.listarNiveles();
    this.listarIdiomas();
  }

  listarIdiomas()
  {
    this.IdiomaService.listarIdiomasActivos().subscribe((data:any)=>{
      this.idiomas=data.content;
    })
  }

  listarNiveles()
  {
      this.nivelService.listarNiveles().subscribe((data:any)=>{
        this.niveles=data.content;
      })
  }

  onIdiomaSelected(idioma) {
    console.log('seleccionado idioma');
    this.idioma = idioma;

    if (this.idioma != '' && this.nivel != '') {
      this.cargarExamenesUbicacion();
    }
    else {
      console.log('nothign');
    }
  }

  onNivelSelected(nivel) {
    this.nivel = nivel;

    if (this.idioma != '' && this.nivel != '') {
      this.cargarExamenesUbicacion();
    }
    else {
      console.log('nothign');
    }
  }

  cargarExamenesUbicacion() {
    console.log('cargando examenes de ubicacion');

    this.examenUbicacionservice.findByNivelAndIdioma(this.nivel, this.idioma).subscribe((data: any) => {
      this.examenes=data.content
      console.log(data.content);
    })
  }



}
