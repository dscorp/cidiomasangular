import { Injectable } from '@angular/core';
import { RestClient } from 'src/app/Util/restClient';
import { HttpClient } from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class GrupoService {

  path:any = RestClient.URL_SERVER + '/grupo/';

  constructor(
    private http:HttpClient,
    private auth: AuthService
  ) { }

  listar(){
    if(this.auth.currentUserValue.tipoUsuario=='administrativo'){
      return this.http.get(this.path);
    }else{
      return this.http.get(this.path);
    }
  }


listarByCicloAndModalidad(idCiclo:string, idModalidad:string)
{
  return this.http.get(this.path+'getByCicloAndModalidad/'+idCiclo+'/'+idModalidad)
}

  save(grupo:any)
  {
    return this.http.post(this.path,grupo);
  }

}
