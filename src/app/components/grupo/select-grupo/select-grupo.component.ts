import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { GrupoService } from '../grupo.service';

import { MatSelectionList, MatListOption } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { CicloService } from '../../componentsgeneral/ciclo/ciclo.service';
import { ModalidadService } from '../../componentsgeneral/modalidad/modalidad.service';
import { NivelService } from '../../componentsgeneral/nivel/nivel.service';
import { IdiomaService } from '../../componentsgeneral/idioma/idioma.service';

@Component({
  selector: 'app-select-grupo',
  templateUrl: './select-grupo.component.html',
  styleUrls: ['/select-grupo.component.css']

})
export class SelectGrupoComponent implements OnInit {

  @ViewChild(MatSelectionList, { static: true })
  private selectionList: MatSelectionList;

  controlGrupo: FormControl;

  formGroupHelper: FormGroup;


  @Input()
  inFromGroup: FormGroup;

  @Output()
  OutIdGrupoEmmiter = new EventEmitter<string>();

  @Output()
  outNivelIdiomaModalidadEmitter = new EventEmitter<any>();


  @Output() modalidadEmmiter = new EventEmitter<any>();

  @Output() idiomaEmmiter  = new EventEmitter<any>();

  grupos: any[] = []
  ciclos: any[] = []
  cicloFromEmmiter: any = '';
  modalidadFromEmmiter: any = '';
  idioma: any = '';
  nivel: any = '';

  modalidades: any[] = []
  niveles: any[] = []
  idiomas: any[] = []
  constructor(
    private grupoService: GrupoService
    , private fbuilder: FormBuilder
    , private cicloService: CicloService
    , private modalidadService: ModalidadService
    , private nivelService: NivelService
    , private IdiomaService: IdiomaService
  ) {


    //@Carrillo
    //form que sirve para ayudar con los validadores de los componentes reutilizables

    this.formGroupHelper = fbuilder.group({
      'idModalidad': ['', Validators.required],
      'idNivel': ['', Validators.required],
      'idIdioma': ['', Validators.required],
      'idCiclo': ['', Validators.required]
    })
    // this.controlGrupo.setValidators(Validators.required);

  }


  listarIdiomas() {
    this.IdiomaService.listarIdiomasActivos().subscribe((data: any) => {
      this.idiomas = data.content;
    })
  }
  listarNiveles() {
    this.nivelService.listarNiveles().subscribe((data: any) => {
      this.niveles = data.content;
    })
  }

  onListSelect($event) {

    if ($event.option.selected === true) {
      let idGrupo = $event.option.value;
      this.OutIdGrupoEmmiter.emit(idGrupo);
    } else {

    }

  }
  ngOnInit() {
    this.selectionList.selectedOptions = new SelectionModel<MatListOption>(false);

    this.listarModalidades();


  }


  listarModalidades() {
    this.modalidadService.getAll().subscribe((data: any) => {
      this.modalidades = data.content;
    })
  }



  onModalidadSelected(idModalidad) {

    this.modalidadEmmiter.emit(idModalidad);

    this.formGroupHelper.get('idIdioma').setValue('')
    this.formGroupHelper.get('idNivel').setValue('')
    this.formGroupHelper.get('idCiclo').setValue('')

    this.idiomas = [];
    this.niveles = [];
    this.ciclos = [];
    this.grupos = [];
    if (idModalidad != '') {

      this.modalidadFromEmmiter = idModalidad;
      this.listarIdiomas();
    } else {
    }

  }

  onIdiomaSelected(idioma) {
    this.idiomaEmmiter.emit(idioma);
    this.niveles = [];
    this.ciclos = [];
    this.grupos = [];
    this.formGroupHelper.get('idNivel').setValue('')
    this.formGroupHelper.get('idCiclo').setValue('')
    if (idioma != '') {
      this.idioma = idioma;
      this.listarNiveles();
    } else {
    }
  }

  onNivelSelected(nivel) {

    this.ciclos = [];
    this.grupos = [];

    this.formGroupHelper.get('idCiclo').setValue('')
    this.inFromGroup.get('idGrupo').setValue('');

    if (nivel != '') {
      this.nivel = nivel;


      if (this.idioma != '' && this.nivel != '') {
        let obj = {
          "idNivel": this.nivel,
          "idModalidad": this.modalidadFromEmmiter,
          "idIdioma": this.idioma
        }


        this.outNivelIdiomaModalidadEmitter.emit(obj);

        this.cargarCiclosByIdiomaAndNivel();

      }

    } else {
      this.outNivelIdiomaModalidadEmitter.emit('');
    }

  }



  onCicloselected(ciclo) {

    this.grupos = [];
    this.inFromGroup.get('idGrupo').setValue('');
    if (ciclo != '') {
      this.cicloFromEmmiter = ciclo;
      this.listarGruposByCicloAndModalidad();

    } else {
    }

  }

  //el ciclo ya tiene un idioma y nivel
  listarGruposByCicloAndModalidad() {
    if (this.modalidadFromEmmiter != '' && this.cicloFromEmmiter != '')
      this.grupoService.listarByCicloAndModalidad(this.cicloFromEmmiter, this.modalidadFromEmmiter).subscribe((data: any) => {
        console.log('grupos');
        console.log(data.content);
        this.grupos = data.content;
      })
  }

  cargarCiclosByIdiomaAndNivel() {
    this.cicloService.getByIdiomaAndNivel(this.idioma, this.nivel).subscribe((data: any) => {
      this.ciclos = data.content;
    })
    // this.examenUbicacionservice.findByNivelAndIdioma(this.nivel, this.idioma).subscribe((data: any) => {
    //   this.examenes = data.content
    //   console.log(data.content);
  }


}
