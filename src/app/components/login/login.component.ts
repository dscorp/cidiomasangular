import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/model/models';
import { LoginService } from './login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public usuario : Usuario = new Usuario();
  public titulo:string ='Iniciar Session';
  public loginForm: FormGroup;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public errormessaje:string;



  constructor(
    private loginservice: LoginService,
    private route: ActivatedRoute,
    private router:Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,) {
  }

  ngOnInit() {  
    this.loginForm = this.formBuilder.group({
      idUsuario: [''],
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

  login(){
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    this.loading = true;
    setTimeout( () => { 
      this.authService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
          data => {
            this.loading=false;
            this.router.navigate([this.returnUrl]);
          },
          error => {
              console.log("ERROR " + error);
              this.errormessaje = error;
              this.loading = false;
          });

     }, 1000 );

  }
}
