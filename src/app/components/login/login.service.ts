import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';
import { Usuario } from 'src/app/model/models';

@Injectable({
  providedIn: 'root'
})
export class LoginService {


  path:any = RestClient.URL_SERVER+'/usuario';
  constructor(private http:HttpClient) { }



  login(username: any, password: any) {
   

    let usuario = new Usuario();
   usuario.username=username;
   usuario.password=password;
   const config = { headers: new HttpHeaders().set('Content-Type', 'application/json') };
    return this.http.post(this.path+'/login',usuario,config);
  }

}
