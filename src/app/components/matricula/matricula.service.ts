import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';
import { DetallePago, HelperMatricula } from 'src/app/model/models';

@Injectable({
  providedIn: 'root'
})
export class MatriculaService {
  path: any = RestClient.URL_SERVER + '/matricula/';
  constructor(private http: HttpClient) {

  }


  registrarMatricula(matricula:any, detpagoCarnet?:DetallePago){

     let objmatricula = new HelperMatricula();

     objmatricula.matricula=matricula;
     objmatricula.detpagoCarnet=detpagoCarnet;

     console.log('enviando matricula');
    console.log(JSON.stringify(objmatricula));

    return this.http.post(this.path,objmatricula);
  }
}
