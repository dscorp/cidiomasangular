import { Component, OnInit, Input } from '@angular/core';
import { NiveldeidiomaService } from './niveldeidioma.service';
import Swal from 'sweetalert2';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-nivel-idioma',
  template:`
  
  <div  [formGroup]="parentForm">
    <!-- (change)="listarProvinciaUnDepartamento($event)" -->
    <select class="form-control" id="nivel"  formControlName="idNivelIdioma">
      <option value="">Seleccion Un Idioma-Nivel</option>
      <option *ngFor="let n of nivelesIdioma" value="{{n.idNivelIdioma}}">{{n.idioma.nombre}} - {{n.nivel.nombre}}
      </option>
    </select>

    <div class=" text-danger" *ngIf="parentForm.controls['idNivelIdioma'].errors?.required" >
      Debe elegir un Nivel-Idioma
    </div>
  </div>
  `
})
export class NivelIdiomaComponent implements OnInit {
  nivelesIdioma:any[]=[];


  @Input()
  parentForm:FormGroup;

  constructor(

    private nivelIdiomaService:NiveldeidiomaService

  ) { 

    this.listarNivelesIdioma();
  }

    listarNivelesIdioma()
    {
      this.nivelIdiomaService.getNivelesidioma().subscribe((data:any)=>{
 
          this.nivelesIdioma=data.content;
   
      },err=>{
        Swal.fire('ERROR',err.error.mensaje, 'error')
      })
    }



  ngOnInit() {
  }




}
