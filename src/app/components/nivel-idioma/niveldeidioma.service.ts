import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from '../../Util/restClient';



@Injectable({
  providedIn: 'root'
})
export class NiveldeidiomaService {

  path = RestClient.URL_SERVER+'/nivelidioma/'
  constructor(

private http:HttpClient

  ) { }


  getNivelesByIdioma(id: Number)
  {
    return this.http.get(this.path + 'NivelbyIdioma/' + id);
  }

  getNivelesidioma()
  {
    return this.http.get(this.path);
  }



}
