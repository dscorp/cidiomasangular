import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from '../../Util/restClient';

@Injectable({
  providedIn: 'root'
})
export class PostulacionService {
  path = RestClient.URL_SERVER+'/postulacion'
  constructor(private http:HttpClient) { }



  registrarPostulacion(postulacion:any){
    return this.http.post(this.path,postulacion);
  }
}
