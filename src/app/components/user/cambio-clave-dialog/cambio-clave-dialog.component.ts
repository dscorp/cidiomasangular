import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { FormGroup, FormBuilder, ValidatorFn, ValidationErrors, Validators } from '@angular/forms';
import { UsuarioService } from '../usuario.service';
import { Usuario } from 'src/app/model/models';
import Swal from 'sweetalert2';


@Component({
  selector: 'dialog-password',
  templateUrl: 'cambio-clave-dialog.component.html',
  styleUrls: ['cambio-clave-dialog.component.css']
})
export class CambioClaveDialog {
  hide = true;
formPassword:FormGroup;

  constructor(
    public dialogRef: MatDialogRef<CambioClaveDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
    , private fbuilder:FormBuilder
    , private usuarioService:UsuarioService
    
    ) {
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    this.formPassword= this.fbuilder.group({
      "password1":['',[Validators.required,Validators.minLength(8)]],
      "password2":['',[Validators.required,Validators.minLength(8)]]
    })
    
   
    this.formPassword.setValidators(this.comparisonValidator())
  }

  public comparisonValidator() : ValidatorFn{
    return (group: FormGroup): ValidationErrors => {
       const control1 = group.controls['password1'];
       const control2 = group.controls['password2'];
       if (control1.value !== control2.value) {
          control2.setErrors({notEquivalent: true});
       } else {
          control2.setErrors(null);
       }
       return;
 };

}

actualizarUsuario()
{
 let usuario:Usuario  = new Usuario();
  usuario.idUsuario=this.data.id;
  usuario.password=this.formPassword.get('password2').value;

  this.usuarioService.actualizar(usuario).subscribe((data:any)=>{


    if (!data.error) {
      Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
      // console.log('data devuelta actualizar / registrar');
      // console.log(data.content)
      this.dialogRef.close();
      // this._router.navigate(['alumno'])
      // this.cargando = false;
    }
    else {  
      Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
   
    }


  })
}


}
export interface DialogData {
  nombre: any;
  id: any;
}

