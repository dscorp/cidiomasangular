import { Injectable } from '@angular/core';
import { RestClient } from 'src/app/Util/restClient';
import { HttpClient } from '@angular/common/http';
import { Usuario } from 'src/app/model/models';



@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  path:any =RestClient.URL_SERVER + '/usuario';

  constructor(private http:HttpClient) { }

  listar(){
    return this.http.get(this.path+'/alumno');
  }

  listarUsuarioDocente(){
    return this.http.get(this.path+'/docente')
  }

  listarUsuarioAdministartivo(){
    return this.http.get(this.path+'')
  }

  //search(buscar: Buscar){
   // return this.http.post(this.path+ 'search',buscar);
  //}

  registar(usuario: Usuario){
    return this.http.post(this.path,usuario,RestClient.headerJSON);
  }

  actualizar(usuario: Usuario){
    return this.http.put(this.path, usuario);
  }

  changePass(configuracion : any){
    return this.http.put(this.path+'/changePass', configuracion);
  }
  
}
