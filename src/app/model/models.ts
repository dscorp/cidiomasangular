export class Distrito {
    idDistrito: number;
    nombre: string;
    estado: boolean;
    provincia?: Provincia = new Provincia();
    persona?: any;
}

export class Departamento {
    idDepartamento: number;
    nombre: string;
}

export class Provincia {
    idProvincia: number;
    nombre: string;
    departamento?: Departamento = new Departamento();
}
export class Persona {
    idPersona: number;
    dni: string;
    nombre: string;
    apaterno: string;
    amaterno: string;
    fechaNacimimento?: any;
    sexo: string;
    telefono: string;
    celular: string;
    gradoInstruccion: string;
    foto: string;
    correoElectronico?: any;
    procedencia?: any;
    distrito: Distrito = new Distrito();
    docente?: any;
    administrativos?: any;
}

export class Escuela {
    idEscuela: number;
    nombre: string;
    facultad?: Facultad;
    alumnosUniversitarios?: any;
    estado: boolean;
}

export class AlumnoUniversitario {
    idAlumnoUniversitario: number;
    ciclo: string;
    numeroCarnet: string;
    planDeEstudios: string;
    condicion: string;
    gradoInstruccionActivo: boolean;
    escuela: Escuela = new Escuela();
    alumno: Alumno = new Alumno();
}

export class Colegio {
    idColegio: number;
    nombre: string;
    alumnosColegio?: any;
}

export class AlumnoColegio {
    idAlumnoColegio: number;
    nivel: string;
    grado: string;
    gradoInstruccionActivo: boolean;
    colegio: Colegio = new Colegio();
    alumno: Alumno = new Alumno();
}

export class Usuario {
    idUsuario: number;
    username: string;
    password: string;
    tipoUsuario: string;
    docente?: Docente;
    administrativo?: administrativo;
    alumno?: Alumno
    permisosDeUsuario?: any;

}

export class Alumno {
    idAlumno: number;
    tipoAlumno?: any;
    persona: Persona = new Persona();
    alumnoUniversitario: AlumnoUniversitario;
    alumnoColegio: AlumnoColegio;
    usuario: Usuario = new Usuario();
}

export class Docente {
    idDocente: number;
    activo: boolean;
    montoXhora: number;
    persona: Persona;
    usuario: Usuario;
}

export class Buscar {
    texto: String;
}
export class administrativo {
    idAdministrativo: number;
    tipoAdministrativo?: any;
    persona: Persona;
    usuario: Usuario;
}

export class Facultad {
    idFacultad: number;
    nombre: string;
    universidad?: Universidad;
}

export class Universidad {
    idUniversidad: number;
    nombre: string;
    siglas: string;
}

export class Resultado {
    id: number;
    puntaje: number;
    cicloubicacion: number;
}

export class Postulacion {
    idPostulacion: number;
    alumno: Alumno;
    resultado?: Resultado;
    asistio: boolean;
}

export class Aula {
    idAula: number;
    numero: string;
    piso: string;
    capacidad: number;
    activo: boolean;

}

export class ExamenUbicacion {
    idExamenUbicacion: number;
    numero: number;
    anio: number;
    estado: boolean;
    horaInicio: string;
    horaFin: string;
    aula: Aula;
    docente: Docente;
    serellenoasistencia: boolean;
    serellenopuntuacion: boolean;
}

export class Pago{
    idPago:number;
}

export class DetallePago {
    idDetallePago: number=0;
    numeroRecibo: string='';
    monto: number=0;
    fechaRecibo: any='';
    tipoPago:TipoPago;
    pago?: Pago;
}

export class TipoPago {
    idTipoPago:number;
    precio:number;
    detallesPago:any;
    categoriaTipoPago:any;
    conceptoPago:any;
    tupa:any;
    nivelIdioma:any;
    modalidad:any;
}


export class ConceptoPago{
    idConceptoPago:number;
    conceptoPago:string;
    tiposPago:any;
}


//@carrillo para enviar en un osolo objeto la matricula y su pago de matricula
export class HelperMatricula{
    matricula:any;
    detpagoCarnet:any;
}