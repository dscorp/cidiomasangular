import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListarAdministrativoComponent } from './listar-administrativo/listar-administrativo.component';
import { RegistrarAdministrativoComponent } from './registrar-administrativo/registrar-administrativo.component';
import { MatInputModule, MatTableModule, MatIconModule, MatButtonModule, MatPaginatorModule, MatStepperModule, MatCardModule } from '@angular/material';
import { ComponentsgeneralModule } from '../componentsgeneral/componentsgeneral.module';
import { AdministrativoComponent } from 'src/app/components/administrativo/administrativo.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: 'administrativo',
    children:
      [
        { path: '', component: ListarAdministrativoComponent },
        { path: 'add', component: RegistrarAdministrativoComponent },
      ]
  },
]

@NgModule({
  declarations: [
    ListarAdministrativoComponent,
    RegistrarAdministrativoComponent,
    AdministrativoComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatPaginatorModule,
    MatStepperModule,
    ComponentsgeneralModule,
    MatCardModule,
    RouterModule.forChild(routes),
  ]
})
export class AdministrativoModule { }
