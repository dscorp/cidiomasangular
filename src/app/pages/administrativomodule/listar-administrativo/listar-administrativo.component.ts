import { Component, OnInit, ViewChild } from '@angular/core';
import { Buscar } from 'src/app/model/models';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { AdministrativoService } from '../../../components/administrativo/administrativo.service';

@Component({
  selector: 'app-listar-administrativo',
  templateUrl: './listar-administrativo.component.html',
  styleUrls: ['./listar-administrativo.component.css']
})
export class ListarAdministrativoComponent implements OnInit {

  public buscar = new Buscar();
  administrativo:any[]=[];
  displayedColumns= ['dni','nombre','paterno','materno','tipo','opciones']
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(private adminstrativoService: AdministrativoService) { }

  public adminitrativoResponse: any[] = [];

  ngOnInit() {
    this.listarAdministrativo();
  }

  search(searchValue: String){
    console.log('Input Event ' + searchValue);
    this.buscar.texto = searchValue;
    this.adminstrativoService.Search(this.buscar).subscribe((data:any) =>{
      this.dataSource = new MatTableDataSource(data.content)
    });
  }

  listarAdministrativo(){
    this.adminstrativoService.listar().subscribe((data:any)=>{
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log(data)
      console.log(this.dataSource)
    })
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
