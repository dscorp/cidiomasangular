import { Component, OnInit, ViewChild } from '@angular/core';
import { Usuario, Persona, administrativo } from 'src/app/model/models';
import { AdministrativoService } from '../../../components/administrativo/administrativo.service'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestClient } from '../../../Util/restClient';
import { MatStepper } from '@angular/material/stepper';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { asyncValidatorDNI } from 'src/app/Util/asyncValidatorDNI';

import Swal from 'sweetalert2';
import { PersonaService } from 'src/app/components/componentsgeneral/persona/persona.service';

@Component({
  selector: 'app-registrar-administrativo',
  templateUrl: './registrar-administrativo.component.html',
  styleUrls: ['./registrar-administrativo.component.css']
})
export class RegistrarAdministrativoComponent implements OnInit {



  @ViewChild("stepper", { static: false }) private myStepper: MatStepper;
  @ViewChild('myFinalizado', { static: false }) private myFinalizado: any;

  totalStepsCount: number;
  public usuario = new Usuario();

  administrativoForm: FormGroup;
  id: any;
  cargando: boolean=false;


  
  ngAfterViewInit(): void {
    this.totalStepsCount = this.myStepper._steps.length;
  }
  
  constructor(
    fBuilder: FormBuilder
    , private administrativoService: AdministrativoService
    , private _activatedRoute: ActivatedRoute
    , private personaService:PersonaService
    ,  private _router: Router,
    
    ) {
      this.administrativoForm = fBuilder.group({
        "idAdministrativo":[''],
        "tipoAdministrativo": ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$"), RestClient.noWhitespaceValidator]],
        
        "persona": fBuilder.group({
          "idPersona": [''],
          "nombre": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
          "apaterno": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
          "amaterno": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
          "dni": ['', [Validators.required, Validators.pattern("^[0-9]+$"), Validators.minLength(8), Validators.maxLength(8)]],
          "fechaNacimimento": ['', Validators.required],
        "sexo": ['', Validators.required],
        "procedencia": ['', [Validators.required, RestClient.noWhitespaceValidator]],
        "telefono": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(7), Validators.maxLength(7),]],
        "celular": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(9), Validators.maxLength(9),]],
        "gradoInstruccion": ['universidad', Validators.required],
        "correoElectronico": ['', [Validators.email]],
        "foto": [''],
        "distrito": fBuilder.group({
          "idDistrito": ['', Validators.required],
          "provincia": fBuilder.group({
            "idProvincia": [''],
            "nombre": [''],
            "departamento": fBuilder.group({
              "idDepartamento": [''],
              "nombre": ['']
            })
          })
          
        }),
      }),
      
      "usuario": fBuilder.group({
        "idUsuario": [''],
        "username": ['', [Validators.minLength(6), Validators.pattern("^[a-zA-Z0-9]+$")]],
        "password": ['', [Validators.minLength(8)]],
        "tipoUsuario": ['universidad']
      })
    })
    
    
    this._activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      if (this.id == null) {
        
        this.administrativoService.registrando=true;
      }
    })
  }
  
  ngOnInit() {
    console.log('onInit() registrar administrativo');
    if (this.administrativoService.registrando == false) {

      this.administrativoForm.get('persona').get('dni').setAsyncValidators(asyncValidatorDNI.createValidatorActualizar(this.personaService))
      //esto busca el alumno y lo carga en el formulatio
      this.administrativoService.findById(this.id).subscribe((data: any) => {
        //patchvalue rellena todos los datos traidos en el formgroup principal
        this.administrativoForm.patchValue(data.content);
      })
    }
    else {
      this.administrativoForm.get('persona').get('dni').setAsyncValidators(asyncValidatorDNI.createValidatorRegistrar(this.personaService))

      this.administrativoForm.get('persona').get('dni').valueChanges.subscribe(val => {
        this.administrativoForm.get('usuario').get('username').setValue(val)
        this.administrativoForm.get('usuario').get('password').setValue(val)
      });
    }

  }

  request: Observable<any>;
  submit(formValue) {

    if (this.administrativoService.registrando) {
      this.cargando = true;
      this.request = this.administrativoService.registrar(formValue);
    }
    else {
      this.cargando = true;
      this.request = this.administrativoService.actualizar(formValue)
    }


    this.request.subscribe((data: any) => {
      if (!data.error) {
        Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
        // console.log('data devuelta actualizar / registrar');
        // console.log(data.content)
        this._router.navigate(['administrativo'])
        this.cargando = false;
      }
      else {
        Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
        this.cargando=false;
      }

    }, error => {
      this.cargando = false;
      console.log(error)
      Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' + error.error.mensaje, 'error')
    })

  }

  ngOnDestroy(): void {
    this.administrativoService.registrando=false;
  }

}

