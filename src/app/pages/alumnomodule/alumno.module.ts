import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColegioComponent } from 'src/app/components/alumno/colegio/colegio.component';
import { EscuelaComponent } from 'src/app/components/alumno/escuela/escuela.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatStepperModule, MatRadioModule, MatTableModule, MatFormFieldModule, MatIconModule, MatPaginatorModule, MatInputModule, MatButtonModule, MatCardModule, MatAutocomplete, MatAutocompleteModule, MatOptionModule } from '@angular/material';
import { ComponentsgeneralModule } from '../componentsgeneral/componentsgeneral.module';
import { BaseListarAlumnosComponent } from 'src/app/components/alumno/base-listar-alumnos/base-listar-alumnos.component';
import { FormAlumnoColegioComponent } from 'src/app/components/alumno/form-alumno-colegio/form-alumno-colegio.component';
import { FormAlumnoUniversitarioComponent } from 'src/app/components/alumno/form-alumno-universitario/form-alumno-universitario.component';
import { PagoModule } from '../pagomodule/pago.module';
import { RouterModule, Routes } from '@angular/router';
import { ListarAlumnosComponent } from './listar-alumnos/listar-alumnos.component';
import { EditarAlumnoUnivComponent } from './editar-alumno-univ/editar-alumno-univ.component';
import { EditarAlumnoColegioComponent } from './editar-alumno-colegio/editar-alumno-colegio.component';
import { AutocompleteAlumnoComponent } from 'src/app/components/alumno/autocomplete-alumno/autocomplete-alumno.component';

const routes: Routes = [
  {
    path: 'alumno', children:
      [
        { path: 'listar', component: ListarAlumnosComponent },
        { path: 'editAlUniv/:id', component: EditarAlumnoUnivComponent },
        { path: 'editAlCol/:id', component: EditarAlumnoColegioComponent },

      ]
  }
]

@NgModule({
  declarations: [
    AutocompleteAlumnoComponent,
    EscuelaComponent,
    ColegioComponent,
    BaseListarAlumnosComponent,
    FormAlumnoColegioComponent,
    FormAlumnoUniversitarioComponent,
    ListarAlumnosComponent,
    EditarAlumnoColegioComponent,
    EditarAlumnoUnivComponent,
  ],
  exports: [
    AutocompleteAlumnoComponent,
    EscuelaComponent,
    ColegioComponent,
    FormAlumnoColegioComponent,
    FormAlumnoUniversitarioComponent,
    BaseListarAlumnosComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(routes),
    //modulos del proyecto
    ComponentsgeneralModule,
    //Modulos de angular
    ReactiveFormsModule,
    FormsModule,
    MatStepperModule,
    MatRadioModule,
    MatTableModule,
    MatFormFieldModule,
    MatIconModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatOptionModule,
  ]
})


export class AlumnoModule { }
