import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { RestClient } from '../../../Util/restClient';
import { Router, ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { asyncValidatorDNI } from '../../../Util/asyncValidatorDNI';
import { AlumnoService } from 'src/app/components/alumno/alumno.service';
import { PersonaService } from 'src/app/components/componentsgeneral/persona/persona.service';







@Component({
  selector: 'app-editar-alumno-colegio',
  templateUrl: './editar-alumno-colegio.component.html',
  styleUrls: ['./editar-alumno-colegio.component.css']
})
export class EditarAlumnoColegioComponent implements OnInit {

  cargando: boolean = false;

  gradosPrimarios = [1, 2, 3, 4, 5, 6];
  gradosSecundarios = [1, 2, 3, 4, 5];

  alumnoColegio: FormGroup;
  id: any;
  grados: any[] = [];
  public entidadForm: FormGroup;

  constructor(
    fBuilder: FormBuilder,
    private personaService: PersonaService,
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private alumnoService: AlumnoService

  ) {

    this.alumnoColegio = fBuilder.group({
      "idAlumnoColegio": [''],
      "grado": ['', [Validators.required]],
      "nivel": ['', [Validators.required]],
      "gradoInstruccionActivo": true,

      "colegio": fBuilder.group({
        "idColegio": ['', Validators.required]
      }),
      "alumno": fBuilder.group({
        "idAlumno": [''],
        "tipoAlumno": ['colegio', Validators.required],
        "persona": fBuilder.group({
          "idPersona": [''],
          "nombre": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
          "apaterno": ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$"), RestClient.noWhitespaceValidator]],
          "amaterno": ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$"), RestClient.noWhitespaceValidator]],
          "dni": ['', [Validators.required, Validators.pattern("^[0-9]+$"), Validators.minLength(8), Validators.maxLength(8)]],
          "fechaNacimimento": ['', Validators.required],
          "sexo": ['', Validators.required],
          "procedencia": ['', [Validators.required, RestClient.noWhitespaceValidator]],
          "telefono": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(7), Validators.maxLength(7),]],
          "celular": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(9), Validators.maxLength(9),]],
          "gradoInstruccion": ['colegio', Validators.required],
          "correoElectronico": ['', [Validators.email]],
          "foto": [''],
          "distrito": fBuilder.group({
            "idDistrito": ['', Validators.required],
            "provincia": fBuilder.group({
              "idProvincia": [''],
              "departamento": fBuilder.group({
                "idDepartamento": [''],
              })
            })
          }),
        }),

        "usuario": fBuilder.group({
          "idUsuario": [''],
          "username": ['', [Validators.minLength(6), Validators.pattern("^[a-zA-Z0-9]+$")]],
          "password": ['', [Validators.minLength(8)]],
          "tipoUsuario": ['alumno']
        })
      }),
    })

    this._activatedRoute.params.subscribe(params => {
      this.id = params['id']
      if (this.id == null) {
        this.alumnoService.registrando = true;
      }
    })

  }

  listargrados(e) {

    //agregar  linea que  setee valor por defecto
    this.alumnoColegio.get('grado').reset();

    switch (e.target.value) {
      case "":
        this.grados = null;
        break;
      case "primario":
        this.grados = this.gradosPrimarios
        break;
      case "secundario":
        this.grados = this.gradosSecundarios
        break;
    }


  }


  ngOnInit() {
    console.log('ejecutando onInit');
    if (this.alumnoService.registrando == false) {
      this.alumnoColegio.get('alumno').get('persona').get('dni').setAsyncValidators(asyncValidatorDNI.createValidatorActualizar(this.personaService))
      //esto busca el alumno y lo carga en el formulatio
      this.alumnoService.findAlumnoColvById(this.id).subscribe((data: any) => {
console.log('estamos actualizando');
        //patchvalue rellena todos los datos traidos en el formgroup principal
console.log(data.content);
        this.alumnoColegio.patchValue(data.content);

        let nivel = this.alumnoColegio.get('nivel').value;

        if (nivel == 'secundario') {
          this.grados = this.gradosPrimarios
        }
        else {
          this.grados = this.gradosSecundarios
        }

      })
    }

    else {

      this.alumnoColegio.get('alumno').get('persona').get('dni').setAsyncValidators(asyncValidatorDNI.createValidatorRegistrar(this.personaService))

      this.alumnoColegio.get('alumno').get('persona').get('dni').valueChanges.subscribe(val => {
        this.alumnoColegio.get('alumno').get('usuario').get('username').setValue(val)
        this.alumnoColegio.get('alumno').get('usuario').get('password').setValue(val)
      });
    }



  }

  ngOnDestroy(): void {



    this.alumnoService.registrando = false;
    console.log('onDestroy()');
    // console.log(this.alumnoColegioService.registrando=false);
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

  }
  // submit() {
  //     this.cargando=true;
  //     // setTimeout(() => {
  //     // }, 5000);

  //   this.alumnoService.registrarAlumnoColegio(this.alumnoColegio.value).subscribe((data:any)=>{
  //    if(!data.error){
  //     Swal.fire('Exito', 'Registro correcto', 'success')
  //     this._router.navigate(['alumno'])
  //   this.cargando=false;
  //     console.log('respuesta al registro de alumno  ')
  //     console.log(data)
  //    }
  //    else{
  //     Swal.fire('Error', 'Ocurrio un error al intentar registrar el alumno: '+data.mensaje, 'error')
  //    }

  //   },error=>{
  //     console.log(error)
  //     Swal.fire('Error', 'Ocurrio un error al intentar registrar el alumno: '+error.error.message, 'error')
  //   })

  // }
  peticion: Observable<any>;
  submit(formValue) {
    if (this.alumnoService.registrando) {
      this.cargando = true;
      this.peticion = this.alumnoService.registrarAlumnoColegio(formValue);
    }
    else {
      this.cargando = true;
      // this.alumnoColegio.get('usuario').setValue(null)
      this.peticion = this.alumnoService.actualizarAlumnoCol(formValue)
    }

    this.peticion.subscribe((data: any) => {
      if (!data.error) {
        Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
        // console.log('data devuelta actualizar / registrar');
        // console.log(data.content)
        this._router.navigate(['alumno'])
        this.cargando = false;
      }
      else {
        console.log('este es el error');
        console.log(data);
        Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
        this.cargando = false;
      }

    }, error => {
      console.log(error)
      Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' + error.error.mensaje, 'error')
      this.cargando = false;
    })


  }




}
