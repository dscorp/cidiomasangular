import { Component, OnInit } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, FormGroup, Validators, NgForm, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import { RestClient } from '../../../Util/restClient';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { asyncValidatorDNI } from 'src/app/Util/asyncValidatorDNI';
import { AlumnoService } from 'src/app/components/alumno/alumno.service';
import { PersonaService } from 'src/app/components/componentsgeneral/persona/persona.service';



@Component({
  selector: 'app-editar-alumno-univ',
  templateUrl: './editar-alumno-univ.component.html',
  styleUrls: ['./editar-alumno-univ.component.css']
})
export class EditarAlumnoUnivComponent implements OnInit {

  cargando: boolean = false;
  alumnoUniversidad: FormGroup;
  id: any;

  constructor(fBuilder: FormBuilder
    , private alumnoService: AlumnoService
    , private personaService: PersonaService
    , private _router: Router
    , private _activatedRoute: ActivatedRoute
  ) {

    this.alumnoUniversidad = fBuilder.group({
      "idAlumnoUniversitario": [],
      "ciclo": ['', [Validators.required, Validators.pattern("^[0-9]+$")]],
      "planDeEstudios": ['', [Validators.required, Validators.pattern("^[0-9]+$")]],
      "condicion": ['', Validators.required],
      "gradoInstruccionActivo": true,
      "escuela": fBuilder.group({
        "idEscuela": ['', Validators.required],
        "nombre": [''],
        "facultad": fBuilder.group({
          "idFacultad": [''],
          "nombre": [''],
          "universidad": fBuilder.group({
            "idUniversidad": ['', Validators.required],
            "nombre": [''],
            "siglas": ['']
          })
        })
      }),
      "alumno": fBuilder.group({
        "idAlumno": [''],
        "tipoAlumno": ['',Validators.required],//este valor no representa nada, el tipo de alumno sera asignado en el backend  de acuerdo a su universidad o colegio
        "codigoAlumno": [''],
        "persona": fBuilder.group({
          "idPersona": [''],
          "nombre": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
          "apaterno": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
          "amaterno": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
          "dni": ['', [Validators.required, Validators.pattern("^[0-9]+$"), Validators.minLength(8), Validators.maxLength(8)]],
          "fechaNacimimento": ['', Validators.required],
          "sexo": ['', Validators.required],
          "procedencia": ['', [Validators.required, RestClient.noWhitespaceValidator]],
          "telefono": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(7), Validators.maxLength(7),]],
          "celular": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(9), Validators.maxLength(9),]],
          "gradoInstruccion": ['universidad', Validators.required],
          "correoElectronico": ['', [Validators.email]],
          "foto": [''],
          "distrito": fBuilder.group({
            "idDistrito": ['', Validators.required],
            "provincia": fBuilder.group({
              "idProvincia": [''],
              "nombre": [''],
              "departamento": fBuilder.group({
                "idDepartamento": [''],
                "nombre": ['']
              })
            })

          }),
        }),

        "usuario": fBuilder.group({
          "idUsuario": [''],
          "username": ['', [Validators.minLength(6), Validators.pattern("^[a-zA-Z0-9]+$")]],
          "password": ['', [Validators.minLength(8)]],
          "tipoUsuario": ['alumno']
        })
      }),



    })

    this._activatedRoute.params.subscribe(params => {
      this.id = params['id']
      if (this.id == null) {
        this.alumnoService.registrando = true;
      }
    })

  }
  ngOnInit() {


    if (this.alumnoService.registrando == false) {

      this.alumnoUniversidad.get('alumno').get('persona').get('dni').setAsyncValidators(asyncValidatorDNI.createValidatorActualizar(this.personaService))
      //esto busca el alumno y lo carga en el formulatio
      this.alumnoService.findAlumnoUnivById(this.id).subscribe((data: any) => {
        //patchvalue rellena todos los datos traidos en el formgroup principal
        this.alumnoUniversidad.patchValue(data.content);


      })
    }
    else {

      this.alumnoUniversidad.get('alumno').get('persona').get('dni').setAsyncValidators(asyncValidatorDNI.createValidatorRegistrar(this.personaService))
    }

  }


  peticion: Observable<any>;
  submit(formValue) {
    console.log(this.alumnoUniversidad)
    if (this.alumnoService.registrando) {
      this.cargando = true;
      this.peticion = this.alumnoService.registrarAlumnoUniversidad(formValue);
    }
    else {
      this.cargando = true;
      this.peticion = this.alumnoService.actualizarAlumnoUniv(formValue)
    }

    this.peticion.subscribe((data: any) => {
      if (!data.error) {
        Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
        console.log('data devuelta actualizar / registrar');
        console.log(data.content)
        this._router.navigate(['alumno'])
        this.cargando = false;
      }
      else {
        Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
        this.cargando = false;
      }

    }, error => {
      console.log(error)
      Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' + error.error.mensaje, 'error')
      this.cargando = false;
    })


  }


  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.

    this.alumnoService.registrando = false;
  }

  ngAfterViewInit(): void {
    //Esto sirve para que se copie el valor del campo dni en username y password de usuario
    this.alumnoUniversidad.get('alumno').get('persona').get('dni').valueChanges.subscribe(val => {
      // console.log(this.alumnoUniversidad);
      this.alumnoUniversidad.get('alumno').get('usuario').get('username').setValue(val)
      this.alumnoUniversidad.get('alumno').get('usuario').get('password').setValue(val)
    });
  }


  ex() {

    const invalid = [];
    const controls = this.alumnoUniversidad.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    console.log(invalid);

    console.log(this.alumnoUniversidad);
  }



}
