import { Component, OnInit} from '@angular/core';
import { AlumnoService } from '../../../components/alumno/alumno.service';


@Component({
  selector: 'app-listar-alumnos',
  templateUrl: './listar-alumnos.component.html',
  styleUrls: ['./listar-alumnos.component.css']
})
export class ListarAlumnosComponent implements OnInit {
  
  content:any;

  constructor(private alumnoService:AlumnoService) { 
    this.listar();
  }

  ngOnInit() {
    this.listar();
  }

  listar()
  {
    this.alumnoService.listarAlumnos().subscribe((data:any)=>{
      this.content = data.content;
    });

  }

}
