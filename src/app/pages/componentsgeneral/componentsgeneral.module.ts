import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AulaComponent } from '../../components/componentsgeneral/aula/aula.component';
import { ModalidadComponent } from '../../components/componentsgeneral/modalidad/modalidad.component';
import { NivelComponent } from '../../components/componentsgeneral/nivel/nivel.component';
import { IdiomaComponent } from '../../components/componentsgeneral/idioma/idioma.component';
import { HorarioComponent } from '../../components/componentsgeneral/horario/horario.component';
import { DiaComponent } from '../../components/componentsgeneral/horario/dia/dia.component';
import { CicloComponent } from '../../components/componentsgeneral/ciclo/ciclo.component';
import { PersonaComponent } from '../../components/componentsgeneral/persona/persona.component';
import { GeoubicacionComponent } from '../../components/componentsgeneral/geoubicacion/geoubicacion.component';
import { DialogDNiExistComponent } from '../../components/componentsgeneral/persona/dialogDNIExist/dialog-dni-exist.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { ColegioComponent } from 'src/app/components/alumno/colegio/colegio.component';



@NgModule({
  declarations: [
    AulaComponent,
    ModalidadComponent,
    NivelComponent,
    IdiomaComponent,
    HorarioComponent,
    CicloComponent,
    PersonaComponent,
    GeoubicacionComponent,
    DialogDNiExistComponent,
    DiaComponent,
    
  ],
  
  exports: [
    AulaComponent,
    ModalidadComponent,
    NivelComponent,
    IdiomaComponent,
    HorarioComponent,
    CicloComponent,
    PersonaComponent,
    GeoubicacionComponent,
  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
  ]
})
export class ComponentsgeneralModule { }
