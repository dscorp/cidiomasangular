import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutocompleteDocenteComponent } from '../../components/docente/autocomplete-docente/autocomplete-docente.component';
import { FormDocenteComponent } from '../../components/docente/form-docente/form-docente.component';
import { ComboDocenteComponent } from '../../components/docente/combo-docente/combo-docente.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatAutocompleteModule, MatInputModule, MatTableModule, MatIconModule, MatButtonModule, MatPaginator, MatPaginatorModule, MatStepperModule, MatCard, MatCardModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { ListarDocenteComponent } from './listar-docente/listar-docente.component';
import { RegistrarDocenteComponent } from './registrar-docente/registrar-docente.component';
import { ComponentsgeneralModule } from '../componentsgeneral/componentsgeneral.module';

const routes: Routes = [
  {
    path: 'docente',
    children:
      [
        { path: '', component: ListarDocenteComponent },
        { path: 'add', component: RegistrarDocenteComponent },
      ]
  },
]

@NgModule({
  declarations: [
    AutocompleteDocenteComponent,
    FormDocenteComponent,
    ComboDocenteComponent,
    ListarDocenteComponent,
    RegistrarDocenteComponent
  ],
  exports:[
    AutocompleteDocenteComponent,
    FormDocenteComponent,
    ComboDocenteComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatPaginatorModule,
    MatStepperModule,
    ComponentsgeneralModule,
    MatCardModule,
    RouterModule.forChild(routes),
  ]
})
export class DocenteModule { }
