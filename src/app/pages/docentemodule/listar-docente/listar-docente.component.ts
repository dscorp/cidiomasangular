import { Component, OnInit, ViewChild } from '@angular/core';
import { DocenteService } from '../../../components/docente/docente.service';
import { Buscar } from 'src/app/model/models';
import { MatTableDataSource, MatPaginator , MatSort} from '@angular/material';

@Component({
  selector: 'app-listar-docente',
  templateUrl: './listar-docente.component.html',
  styleUrls: ['./listar-docente.component.css']
})
export class ListarDocenteComponent implements OnInit {

  public buscar = new Buscar();
  docentes:any[]=[];
  displayedColumns= ['dni','nombre','apaterno','amaterno','opciones']
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(private docenteService:DocenteService) { }

  public docentesResponse: any[] = [];

  ngOnInit() {
    this.listarDocentes();
  }

  search(searchValue: string){
    console.log('Input Event ' + searchValue);
    this.buscar.texto = searchValue;
    this.docenteService.search(this.buscar).subscribe((data:any) => {
      this.dataSource = new MatTableDataSource(data.content);
    });
  }

  listarDocentes(){
    this.docenteService.listar().subscribe((data:any) => {
      //this.docentesResponse = data.content
      //console.log(data);
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log('datasource para el datatable')
      console.log(this.dataSource)

    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
