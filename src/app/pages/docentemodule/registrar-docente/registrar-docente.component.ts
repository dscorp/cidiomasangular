import { Component, OnInit, AfterViewInit, ViewChild, SimpleChanges } from '@angular/core';
import { Docente, Persona, Usuario } from 'src/app/model/models';
import { DocenteService } from '../../../components/docente/docente.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { RestClient } from '../../../Util/restClient';
import { MatStepper } from '@angular/material/stepper';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { asyncValidatorDNI } from 'src/app/Util/asyncValidatorDNI';
import { PersonaService } from 'src/app/components/componentsgeneral/persona/persona.service';


@Component({
  selector: 'app-registrar-docente',
  templateUrl: './registrar-docente.component.html',
  styleUrls: ['./registrar-docente.component.scss']
})
export class RegistrarDocenteComponent implements OnInit, AfterViewInit {

  @ViewChild("stepper", { static: false }) private myStepper: MatStepper;
  //@ViewChild('finalizado', {static:false}) private myFinalizado: any;

  totalStepsCount: number;
  public docenteForm: FormGroup;
  public registrando: boolean = false;
  public cargando: boolean = false;
  public id: any;


  ngAfterViewInit(): void {
    this.totalStepsCount = this.myStepper._steps.length;
  }

  constructor(
    fBuilder: FormBuilder
    , private docenteService: DocenteService
    , private _router: Router
    , private _activatedRoute: ActivatedRoute
    , private personaService: PersonaService

  ) {
    this.docenteForm = fBuilder.group({
      "idDocente": [''],
      "montoXhora": ['', [Validators.required]],
      "activo":[true],

      "persona": fBuilder.group({
        "idPersona": [''],
        "nombre": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
        "apaterno": ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$"), RestClient.noWhitespaceValidator]],
        "amaterno": ['', [Validators.required, Validators.pattern("^[a-zA-Z ]*$"), RestClient.noWhitespaceValidator]],
        "dni": ['', [Validators.required, Validators.pattern("^[0-9]+$"), Validators.minLength(8), Validators.maxLength(8)]],
        "fechaNacimimento": ['', Validators.required],
        "sexo": ['', Validators.required],
        "procedencia": ['', [Validators.required, RestClient.noWhitespaceValidator]],
        "telefono": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(7), Validators.maxLength(7),]],
        "celular": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(9), Validators.maxLength(9),]],
        "gradoInstruccion": ['universidad', [Validators.required]],
        "correoElectronico": ['', [Validators.email]],
        "foto": [''],
        "distrito": fBuilder.group({
          "idDistrito": ['', Validators.required],
          "provincia": fBuilder.group({
            "idProvincia": [''],
            "nombre": [''],
            "departamento": fBuilder.group({
              "idDepartamento": [''],
              "nombre": ['']
            })
          })

        }),
      }),
      "usuario": fBuilder.group({
        "idUsuario": [''],
        "username": ['', [Validators.minLength(6), Validators.pattern("^[a-zA-Z0-9]+$")]],
        "password": ['', [Validators.minLength(8)]],
        "tipoUsuario": ['docente']
      })
    })



    this._activatedRoute.params.subscribe(params => {
      this.id = params['id']
      if (this.id == null) {
        this.docenteService.registrando = true;

      }
    })

  }

  ngOnInit() {

    if (this.docenteService.registrando == false) {
      this.docenteForm.get('persona').get('dni').setAsyncValidators(asyncValidatorDNI.createValidatorActualizar(this.personaService))
      //esto busca el alumno y lo carga en el formulatio
      this.docenteService.findById(this.id).subscribe((data: any) => {
        //patchvalue rellena todos los datos traidos en el formgroup principal
        this.docenteForm.patchValue(data.content);
      })

    
    }

    else {

      this.docenteForm.get('persona').get('dni').setAsyncValidators(asyncValidatorDNI.createValidatorRegistrar(this.personaService))

      this.docenteForm.get('persona').get('dni').valueChanges.subscribe(val => {
        this.docenteForm.get('usuario').get('username').setValue(val)
        this.docenteForm.get('usuario').get('password').setValue(val)
      });
    }

  }
  
  
  
  request: Observable<any>;
  submit(formValue) {

    if (this.docenteService.registrando) {
      this.cargando = true;
      this.request = this.docenteService.registar(formValue);
    }
    else {
      this.cargando = true;
      this.request = this.docenteService.actualizar(this.docenteForm.value);
    }

    this.request.subscribe((data: any) => {
      if (data.error===false) {
        Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
        // console.log('data devuelta actualizar / registrar');
        // console.log(data.content)
        this._router.navigate(['docente'])
        this.cargando = false;
      }
      else {
  
        Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
        this.cargando=false;
      }

    }, error => {
      this.cargando = false;
      console.log(error)
      Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' +error.error.mensaje, 'error')
    })
  }

  ngOnDestroy(): void {
    this.docenteService.registrando=false;
  }

}
