import { Component, OnInit, AfterViewInit ,ViewChild } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'app-update-docente',
  templateUrl: './update-docente.component.html',
  styleUrls: ['./update-docente.component.css']
})
export class UpdateDocenteComponent implements OnInit,AfterViewInit {

  @ViewChild("stepper", {static:false}) private myStepper: MatStepper;

  totalStepsCount: number;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.totalStepsCount = this.myStepper._steps.length;
  }

}
