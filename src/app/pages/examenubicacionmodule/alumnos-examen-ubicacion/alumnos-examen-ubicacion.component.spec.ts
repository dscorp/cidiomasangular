import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlumnosExamenUbicacionComponent } from './alumnos-examen-ubicacion.component';

describe('AlumnosExamenUbicacionComponent', () => {
  let component: AlumnosExamenUbicacionComponent;
  let fixture: ComponentFixture<AlumnosExamenUbicacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlumnosExamenUbicacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlumnosExamenUbicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
