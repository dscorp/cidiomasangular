import { Component, OnInit } from '@angular/core';
import { AlumnoService } from '../../../components/alumno/alumno.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-alumnos-examen-ubicacion',
  templateUrl: './alumnos-examen-ubicacion.component.html',
  styleUrls: ['./alumnos-examen-ubicacion.component.css']
})
export class AlumnosExamenUbicacionComponent implements OnInit {

  content:any;
  public id: number;3

  constructor(
    private alumnoService:AlumnoService,
    private  _activatedRoute: ActivatedRoute,
    ) { 

      this._activatedRoute.params.subscribe(params => {
        this.id = params['id']
      })

  }

  ngOnInit() {
    this.listar(this.id);
  }

  listar(id: Number)
  {
    this.alumnoService.listarAlumnosByExamenUbicacion(id).subscribe((data:any)=>{
      this.content = data.content;
    });

  }

}
