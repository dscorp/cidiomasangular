import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsistenciaExamenUbicacionComponent } from './asistencia-examen-ubicacion.component';

describe('AsistenciaExamenUbicacionComponent', () => {
  let component: AsistenciaExamenUbicacionComponent;
  let fixture: ComponentFixture<AsistenciaExamenUbicacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsistenciaExamenUbicacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsistenciaExamenUbicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
