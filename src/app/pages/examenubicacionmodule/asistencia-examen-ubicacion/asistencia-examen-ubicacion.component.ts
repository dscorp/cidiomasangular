import { Component, OnInit, ViewChild } from '@angular/core';
import { Buscar, Postulacion, Alumno, ExamenUbicacion } from 'src/app/model/models';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ExamenubicacionService } from '../../../components/examen-ubicacion/examenubicacion.service';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-asistencia-examen-ubicacion',
  templateUrl: './asistencia-examen-ubicacion.component.html',
  styleUrls: ['./asistencia-examen-ubicacion.component.css']
})
export class AsistenciaExamenUbicacionComponent implements OnInit {

  public buscar = new Buscar();
  public cargando: boolean = false;
  public examenubicacion : ExamenUbicacion;
  public id: number;
  displayedColumns= ['dni','nombre','asistencia']
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<Element>(true, []);
  private postulantes: Array<Postulacion> = [];

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(
    private examenubiService : ExamenubicacionService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) {

      this._activatedRoute.params.subscribe(params => {
        this.id = params['id']
      })

      this.examenubicacion = new ExamenUbicacion();

    }

  ngOnInit() {
    this.get();
  }

  get(){
    this.examenubiService.get(this.id).subscribe((data:any) => {
      this.examenubicacion.idExamenUbicacion = data.content.idExamenUbicacion;
      this.examenubicacion.serellenoasistencia = data.content.serellenoasistencia; 

      this.list();
      //console.log(data.content.idExamenUbicacion);
      //console.log(data.content);
    });
  }

  list(){
    this.examenubiService.alumnos(this.id).subscribe((data:any) => {
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log(data.content);

      if(this.examenubicacion.serellenoasistencia == true){
       
        this.examenubiService.getAsistencia(this.id).subscribe((data:any) => {
          console.log("Asistencia Obtenida");   
          console.log(data.content);

          this.dataSource.data.forEach((row,index) => {

            console.log("La asistencia es ");
            console.log(data.content[index].asistio);

            if(data.content[index].asistio === true){
              this.selection.select(row);
            }

          });
          
        });

      }
    });
  }

  marcarAsistencia(row : any){
    this.selection.toggle(row);
  }

  request: Observable<any>;
  save(){
      this.cargando = true;

      //Crear Postulaciones
      this.dataSource.data.forEach(row => {
        let newPostulacion = new Postulacion();
        newPostulacion.idPostulacion = row.idPostulacion;
        newPostulacion.asistio = this.selection.isSelected(row) ;
        this.postulantes.push(newPostulacion);
      });

      console.log(this.postulantes);
      
      this.request = this.examenubiService.saveAsistencia(this.postulantes);
      
      this.request.subscribe((data: any) => {
        if (data.error===false) {
          Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
          this.cargando = false;
          this.refresh();
        }
        else {
          Swal.fire(data)
          Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
          this.cargando=false;
        }
  
      }, error => {
        this.cargando = false;
        console.log(error)
        Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' +error.error.mensaje, 'error')
      }) 
      
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  refresh(): void {
    window.location.reload();
  }
}
