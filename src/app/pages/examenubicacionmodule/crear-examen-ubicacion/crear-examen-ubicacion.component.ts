import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ExamenubicacionService } from '../../../components/examen-ubicacion/examenubicacion.service';
import Swal from 'sweetalert2';
import { IdiomaService } from 'src/app/components/componentsgeneral/idioma/idioma.service';
import { NivelService } from 'src/app/components/componentsgeneral/nivel/nivel.service';
import { NiveldeidiomaService } from 'src/app/components/nivel-idioma/niveldeidioma.service';

@Component({
  selector: 'app-crear-examen-ubicacion',
  templateUrl: './crear-examen-ubicacion.component.html',
  styleUrls: ['./crear-examen-ubicacion.component.css']
})
export class CrearExamenUbicacionComponent implements OnInit {

  formExamenUbicacion: FormGroup;
  cargando: boolean=false;
  pisos:any[]=[];
  siguientenumero = 0;
  idiomas: any[] = [];
  niveles: any[] = [];
  idiomasFromEmmiter: any = '';

  constructor(
    private fbuilder: FormBuilder,
    private examenUbicacionService: ExamenubicacionService,
    private idiomaService: IdiomaService,
    private nivelidiomaService: NiveldeidiomaService,
  ) {

    this.formExamenUbicacion = fbuilder.group({
      "estado": ['true'],
      "fecha": ['', Validators.required],
      "horaInicio": ['', Validators.required],
      "horaFin": ['', Validators.required],

      "docente": fbuilder.group({
        "idDocente": ['', Validators.required],
      }),

      "nivelIdioma": fbuilder.group({
        "idNivelIdioma": ['', Validators.required]
      }),

      aula:fbuilder.group({
        idAula:['',Validators.required]
      })

    })

    this.obtenersiguientenumero();
  }

  ngOnInit() {
    this.listarIdiomas();
  }

  listarIdiomas(){
    this.idiomaService.listarIdiomasActivos().subscribe((data: any)=>{
      this.idiomas = data.content;
    })
  }

  listarNiveles(id: Number){
    this.nivelidiomaService.getNivelesByIdioma(id).subscribe((data: any)=>{
      this.niveles = data.content;

      console.log("Lista de Niveles");
      console.log(this.niveles);
    })
  }

  onIdiomasSelect(id){
    this.idiomasFromEmmiter = id;

    //Listar Niveles del idioma selecionado
    this.listarNiveles(id);
  }

  onNivelSelect(idNivel){
    this.formExamenUbicacion.get('nivelIdioma').get('idNivelIdioma').setValue(this.getidNivelIdiomaByIdNivel(idNivel));
  }

  submit(fomrvalueorgetrawvalue) {
    this.cargando=true;
    this.examenUbicacionService.save(fomrvalueorgetrawvalue).subscribe((data: any) => {
      Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
      this.cargando=false;
    }, error => {
      Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' + error.error.mensaje, 'error')
      this.cargando=false;
    })
  }

  obtenersiguientenumero() {
    this.examenUbicacionService.obtenersiguientenumero().subscribe((data: any) => {
      this.siguientenumero = data.content;
    })
  }

  getidNivelIdiomaByIdNivel(idNivel: Number){
    return this.niveles.find(n => n.idNivel == idNivel).idNivelIdioma;
  }

  show()
  {
    console.log('formulario');
    console.log(this.formExamenUbicacion);
  }

}
