import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectExUbicacionComponent } from 'src/app/components/examen-ubicacion/select-ex-ubicacion/select-ex-ubicacion.component';
import { ComponentsgeneralModule } from '../componentsgeneral/componentsgeneral.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ListarExamenUbicacionComponent } from './listar-examen-ubicacion/listar-examen-ubicacion.component';
import { CrearExamenUbicacionComponent } from './crear-examen-ubicacion/crear-examen-ubicacion.component';
import { AlumnosExamenUbicacionComponent } from './alumnos-examen-ubicacion/alumnos-examen-ubicacion.component';
import { PuntajeExamenUbicacionComponent } from './puntaje-examen-ubicacion/puntaje-examen-ubicacion.component';
import { AsistenciaExamenUbicacionComponent } from './asistencia-examen-ubicacion/asistencia-examen-ubicacion.component';
import { MatFormFieldModule, MatTableModule, MatIcon, MatIconModule, MatPaginator, MatButtonModule, MatPaginatorModule, MatCheckboxModule, MatInputModule, MatHorizontalStepper, MatStepperModule, MatCardModule, MatRadioModule } from '@angular/material';
import { DocenteModule } from '../docentemodule/docente.module';
import { AlumnoModule } from '../alumnomodule/alumno.module';
import { PagoModule } from '../pagomodule/pago.module';
import { GrupoModule } from '../grupomodule/grupo.module';
import { MatricularPostulanteComponent } from './matricular-postulante/matricular-postulante.component';

/*
Fiel
*/

const routes: Routes = [
  {
    path: 'exubicacion',
    children:
      [
        { path: '', component: ListarExamenUbicacionComponent },
        { path: 'add', component: CrearExamenUbicacionComponent },
        { path: 'postulante', component: MatricularPostulanteComponent},
        { path: ':id/postulantes', component: AlumnosExamenUbicacionComponent },
        { path: ':id/puntaje', component: PuntajeExamenUbicacionComponent },
        { path: ':id/asistencia', component: AsistenciaExamenUbicacionComponent }
      ]
  },
]

@NgModule({
  declarations: [
    ListarExamenUbicacionComponent,
    CrearExamenUbicacionComponent,
    AlumnosExamenUbicacionComponent,
    PuntajeExamenUbicacionComponent,
    AsistenciaExamenUbicacionComponent,
    SelectExUbicacionComponent,
    MatricularPostulanteComponent
  ],

  exports: [
    SelectExUbicacionComponent,
  ],

  imports: [
    CommonModule,
    ComponentsgeneralModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatTableModule,
    MatIconModule,
    MatPaginatorModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    DocenteModule,
    ComponentsgeneralModule,
    AlumnoModule,
    MatStepperModule,
    MatCardModule,
    MatRadioModule,
    PagoModule,
    GrupoModule,
    RouterModule.forChild(routes),    
  ]
})
export class ExamenubicacionModule { }