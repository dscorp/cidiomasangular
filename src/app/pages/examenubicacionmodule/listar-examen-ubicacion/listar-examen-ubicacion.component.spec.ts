import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarExamenUbicacionComponent } from './listar-examen-ubicacion.component';

describe('ListarExamenUbicacionComponent', () => {
  let component: ListarExamenUbicacionComponent;
  let fixture: ComponentFixture<ListarExamenUbicacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarExamenUbicacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarExamenUbicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
