import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ExamenubicacionService } from '../../../components/examen-ubicacion/examenubicacion.service';

@Component({
  selector: 'app-listar-examen-ubicacion',
  templateUrl: './listar-examen-ubicacion.component.html',
  styleUrls: ['./listar-examen-ubicacion.component.css']
})
export class ListarExamenUbicacionComponent implements OnInit {

  displayedColumns = ['id','numero', 'docente','idioma', 'opciones']
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(private examenUbicacionService: ExamenubicacionService) { }

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.examenUbicacionService.list().subscribe((data:any) => {
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
