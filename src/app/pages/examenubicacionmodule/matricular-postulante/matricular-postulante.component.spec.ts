import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatricularPostulanteComponent } from './matricular-postulante.component';

describe('MatricularPostulanteComponent', () => {
  let component: MatricularPostulanteComponent;
  let fixture: ComponentFixture<MatricularPostulanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatricularPostulanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatricularPostulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
