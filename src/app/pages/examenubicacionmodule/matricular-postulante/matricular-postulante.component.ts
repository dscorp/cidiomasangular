import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-matricular-postulante',
  templateUrl: './matricular-postulante.component.html',
  styleUrls: ['./matricular-postulante.component.css']
})
export class MatricularPostulanteComponent implements OnInit {

  formPostulacionExamenUbicacion: FormGroup;

  constructor(
    private fBuilder: FormBuilder
  ) { 

    this.formPostulacionExamenUbicacion = fBuilder.group({

      alumno: fBuilder.group({
        "idAlumno": ['', Validators.required] //sera utilizado el registrado anteriormente
      }),
      examenUbicacion: fBuilder.group({
        "idExamenUbicacion": ['', Validators.required]
      }),
      detallePago: fBuilder.group({
        "idDetallePago": [''],
        "numeroRecibo": ['', Validators.required],
        "monto": ['', Validators.required],
        "fechaRecibo": ['', Validators.required],
        tipoPago: fBuilder.group({
          "idTipoPago": ['', Validators.required]
        }),

        pago: fBuilder.group({
          "idPago": [''],
          alumno: fBuilder.group({
            "idAlumno": ['',]// al llegar al backend sera utilizado el id de arriba esto porque el jsonidentyinfo no  acepta 2 objetos del mismo tipo con el mismo id
          }),
          administrativo: fBuilder.group({
            "idAdministrativo": ['', Validators.required]
          })
        })

      }),
    })

  }

  ngOnInit() {
  }

}
