import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PuntajeExamenUbicacionComponent } from './puntaje-examen-ubicacion.component';

describe('PuntajeExamenUbicacionComponent', () => {
  let component: PuntajeExamenUbicacionComponent;
  let fixture: ComponentFixture<PuntajeExamenUbicacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PuntajeExamenUbicacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PuntajeExamenUbicacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
