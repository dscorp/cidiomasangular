import { Component, OnInit, ViewChild } from '@angular/core';
import { Buscar, Postulacion, Resultado, ExamenUbicacion } from 'src/app/model/models';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Observable } from 'rxjs';
import { ExamenubicacionService } from '../../../components/examen-ubicacion/examenubicacion.service';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-puntaje-examen-ubicacion',
  templateUrl: './puntaje-examen-ubicacion.component.html',
  styleUrls: ['./puntaje-examen-ubicacion.component.css']
})
export class PuntajeExamenUbicacionComponent implements OnInit {

  public buscar = new Buscar();
  private postulantes: Array<Postulacion> = [];
  public cargando: boolean = false;
  public examenubicacion : ExamenUbicacion;
  public id: number;
  public rateControl: FormControl;
  public form :FormGroup;

  displayedColumns= ['dni','nombre','puntaje']
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(
    private examenubiService : ExamenubicacionService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private fb: FormBuilder) {
      this.form = fb.group({});

      this._activatedRoute.params.subscribe(params => {
        this.id = params['id']
      })

      this.examenubicacion = new ExamenUbicacion();
     }

  ngOnInit() {
    this.get();
  }

  get(){
    this.examenubiService.get(this.id).subscribe((data:any) => {
      this.examenubicacion.idExamenUbicacion = data.content.idExamenUbicacion;
      this.examenubicacion.serellenopuntuacion = data.content.serellenopuntuacion; 

      this.list();

    });
  }

  list(){
    this.examenubiService.alumnos(this.id).subscribe((data:any) => {

      let group: any = {};
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      data.content.forEach( (element,index) => {
        group['puntaje_'+index] = new FormControl('',[Validators.required, Validators.min(0), Validators.max(100)]);
       this.form = new FormGroup(group);
      });

      if(this.examenubicacion.serellenopuntuacion == true){
        this.examenubiService.getPuntaje(this.id).subscribe((data:any) => {
          console.log("Puntaje Obtenido");   
          console.log(data.content);
          this.dataSource.data.forEach((row,index) => {
            console.log("El puntaje es ");
            console.log(data.content[index].puntaje);
            this.form.get('puntaje_'+index).setValue(data.content[index].puntaje);
            this.form.disable();
          });
        });
      }


    });

  }

  request: Observable<any>;
  save(){
      this.cargando = true;

            //Crear Postulaciones
            this.dataSource.data.forEach( (row,index) => {
              let newPostulacion = new Postulacion();
              newPostulacion.idPostulacion = row.idPostulacion;
              newPostulacion.resultado = new Resultado();
              newPostulacion.resultado.puntaje = this.form.get("puntaje_"+index).value;
              this.postulantes.push(newPostulacion);
            });
      
            console.log(this.postulantes);
            
            this.request = this.examenubiService.savePuntaje(this.postulantes);
            
            this.request.subscribe((data: any) => {
              if (data.error===false) {
                Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
                this.refresh();
                this.cargando = false;
              }
              else {
                Swal.fire(data)
                Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
                this.cargando=false;
              }
        
            }, error => {
              this.cargando = false;
              console.log(error)
              Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' +error.error.mensaje, 'error')
            }) 
  }
  
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  refresh(): void {
    window.location.reload();
  }
  
}
