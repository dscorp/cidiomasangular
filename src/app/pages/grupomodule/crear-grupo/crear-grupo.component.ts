import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { GrupoService } from '../../../components/grupo/grupo.service';
import Swal from 'sweetalert2';
import { IdiomaService } from 'src/app/components/componentsgeneral/idioma/idioma.service';
import { NivelService } from 'src/app/components/componentsgeneral/nivel/nivel.service';
import { CicloService } from 'src/app/components/componentsgeneral/ciclo/ciclo.service';
import { HorarioService } from 'src/app/components/componentsgeneral/horario/horario.service';
import { ModalidadService } from 'src/app/components/componentsgeneral/modalidad/modalidad.service';


@Component({
  selector: 'app-grupo',
  templateUrl: './crear-grupo.component.html',
  styleUrls: ['./crear-grupo.component.css']
})
export class CrearGrupoComponent implements OnInit {

  

  controlGrupo: FormControl;
  
  // Esqueleto del html ene el component formato json
  formGrupo: FormGroup;


  cargando: boolean=false;

  @Input()
  inFromGroup: FormGroup;

  @Output() 
  OutIdGrupoEmmiter = new EventEmitter<any>();


  modalidades: any[] = []
  idiomas: any[] = []
  niveles: any[] = []
  ciclos: any[] = []
  horarios: any[] = []
  ciclosFromEmmiter: any = '';
  modalidadFromEmmiter: any = '';
  idiomasFromEmmiter: any = '';
  nivelFromEmmiter: any = '';
  horarioFromEmmiter: any = '';

  constructor(

    //Para controllar los campos en el components
    private fbuilder: FormBuilder,
    //llamo al servicio con el metodo
    private grupoSerive: GrupoService,
    private ModalidadService: ModalidadService ,
    private idiomaService: IdiomaService,
    private nivelService: NivelService,
    private cicloService: CicloService,
    private horarioService: HorarioService
  ) {

    //igualas el esqueleto con el controlador del components
    this.formGrupo = fbuilder.group({

      "anio": ['', Validators.required],
      "estado": ['true'],
      "editarNotas":['false'],
      
      "numero": ['',Validators.required],
      "fechaInicio": ['', Validators.required],
      "fechaFinal": ['', Validators.required],

      'modalidad':fbuilder.group({
        "idModalidad":['',Validators.required]
      }),
      'ciclo':fbuilder.group({
        "idCiclo":['',Validators.required]
      }),
      

      "docente": fbuilder.group({
        "idDocente":['',Validators.required]
      }),

      "horario": fbuilder.group({
        "idHorario":['',Validators.required],
        "dia":['',Validators.required],
        "inicio":['',Validators.required],
        "fin":['',Validators.required]
      })

    })
  
  }

  ngOnInit() {
   this.listarModalides();
   this.listarIdiomas();
   this.listarNiveles();
 
  }

  
  listarModalides(){
    this.ModalidadService.getAll().subscribe((data: any) =>{
      this.modalidades = data.content;
      console.log('ddd');
      console.log(data.content);
      
      
    })
  }

  listarIdiomas(){
    this.idiomaService.listarIdiomasActivos().subscribe((data: any)=>{
      this.idiomas = data.content;
    })
  }

  listarNiveles(){
    this.nivelService.listarNiveles().subscribe((data: any)=>{
      this.niveles = data.content;
    })
  }

  listarCiclos(){

    this.cicloService.getByIdiomaAndNivel(this.idiomasFromEmmiter,this.nivelFromEmmiter).subscribe((data: any)=>{
      this.ciclos = data.content;
    })
    
  }
  

  onModalidadesSelect(idModalidad){
    this.modalidadFromEmmiter = idModalidad;
    this.formGrupo.get('modalidad').get('idModalidad').setValue(idModalidad)
    console.log(idModalidad);
  }

  onIdiomasSelect(id){
    this.idiomasFromEmmiter = id;
  
  }

  onNivelSelect(idNivel){
    this.nivelFromEmmiter = idNivel;
    if(this.idiomasFromEmmiter != '' && this.nivelFromEmmiter !=''){
      this.listarCiclos()
    }    
  }

  onCicloSelect(idCiclo){
    this.ciclosFromEmmiter = idCiclo;
    this.formGrupo.get('ciclo').get('idCiclo').setValue(idCiclo)
  }

  onHorarioSelect(idHorario){
    this.horarioFromEmmiter = idHorario;
    this.formGrupo.get('horario').get('idHorario').setValue(idHorario)
  }

  submit(formvalueorgetrawvalue){
    this.cargando=true;
    this.grupoSerive.save(formvalueorgetrawvalue).subscribe((data:any )=>{
      Swal.fire('Exito','Operacion realizada Exitosamente', 'success')
      this.cargando=false;
    }, error =>{
      Swal.fire('error', 'Ocurrio un error al intentar realizar la tarea: ' + error.error.mensaje, 'error')
      this.cargando=false;
    })
  }

}
