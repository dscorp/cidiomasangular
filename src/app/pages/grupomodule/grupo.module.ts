import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectGrupoComponent } from 'src/app/components/grupo/select-grupo/select-grupo.component';
import { MatListModule, MatCardModule, MatListOption, MatFormFieldModule, MatTableModule, MatIconModule, MatButtonModule, MatPaginatorModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsgeneralModule } from '../componentsgeneral/componentsgeneral.module';
import { Routes, RouterModule } from '@angular/router';
import { ListarGrupoComponent } from './listar-grupo/listar-grupo.component';
import { CrearGrupoComponent } from './crear-grupo/crear-grupo.component';
import { DocenteModule } from '../docentemodule/docente.module';

const routes: Routes = [

  {
    path: 'grupo',
    children: [
      { path: '', component: ListarGrupoComponent },
      { path: 'add', component: CrearGrupoComponent }
    ]
  }
  
]

@NgModule({
  declarations: [
    SelectGrupoComponent,
    CrearGrupoComponent ,
    ListarGrupoComponent
  ],
  exports: [SelectGrupoComponent],
  imports: [
    DocenteModule,
    ComponentsgeneralModule,
    CommonModule,


    RouterModule.forChild(routes),
    MatCardModule,
    FormsModule,
    MatListModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatPaginatorModule

  ]
})
export class GrupoModule { }
