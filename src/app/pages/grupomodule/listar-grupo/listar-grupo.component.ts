import { Component, OnInit, ViewChild, NgModule } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { GrupoService } from '../../../components/grupo/grupo.service';
import { characterTransform } from 'src/app/transformchar';

@Component({
  selector: 'app-listar-grupo',
  templateUrl: './listar-grupo.component.html',
  styleUrls: ['./listar-grupo.component.css']
})

@NgModule({
  declarations: [
    characterTransform
  ]
})

export class ListarGrupoComponent implements OnInit {

  displayedColumns = ['id','nombre', 'duracion','detalle','docente', 'opciones']
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;
  
  constructor(private grupoService: GrupoService) { }

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.grupoService.listar().subscribe((data:any) => {
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

}
