import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestClient } from 'src/app/Util/restClient';
import { MatRadioChange, MatStepper } from '@angular/material';
import { AlumnoService } from '../../../components/alumno/alumno.service';
import Swal from 'sweetalert2';
import { TipopagoService } from '../../pagomodule/tipopago/tipopago.service';
import { PostulacionService } from '../../../components/postulacion/postulacion.service';
import { AuthService } from 'src/app/services/auth.service';
import { Usuario } from 'src/app/model/models';
import { MatriculaService } from '../../../components/matricula/matricula.service';
import { asyncValidatorDNI } from 'src/app/Util/asyncValidatorDNI';
import { PersonaService } from 'src/app/components/componentsgeneral/persona/persona.service';



@Component({
  selector: 'app-registro-postulanteo-matricula',
  templateUrl: './crear-matricula.component.html',
  styleUrls: ['./crear-matricula.component.css']
})
export class CrearMatriculaComponent implements OnInit {


  formPostulanteMatricula: FormGroup;
  formPostulacionExamenUbicacion: FormGroup;
  formMatriculaGrupo: FormGroup;
  formDetPagoCarne: FormGroup;

  tipoAlumno: string;
  tipoOperacion: string;
  cargando: boolean = false;
  alumnoRegistradoExitosamente: boolean = false;
  tipospagoExuBicacion: any[] = [];
  tipospagoMatricula: any[] = [];
  tiposPagoCarnet: any[] = [];


  modalidaesGrupo: any[] = [];
  currentUser: Usuario;
  alumnoRegistrado: any;
  formpersonaValido: boolean = false;
  personaCargadaPorValidador = false;


  pagarCarne: boolean = false;

  constructor(
    private alumnoService: AlumnoService,
    private fBuilder: FormBuilder,
    private tipoPagoService: TipopagoService,
    private postulacionService: PostulacionService,
    private authService: AuthService,
    private matriculaService: MatriculaService,
    private personaService: PersonaService
  ) {

    this.currentUser = authService.currentUserValue;

    this.formDetPagoCarne = fBuilder.group
      ({
        "idDetallePago": [''],
        "numeroRecibo": ['', Validators.required],
        "monto": ['', Validators.required],
        "fechaRecibo": ['', Validators.required],
        tipoPago: fBuilder.group
          ({
            "idTipoPago": ['', Validators.required]
          }),
      })

    this.formMatriculaGrupo = fBuilder.group({
      "idMatricula": [''],
      alumno: fBuilder.group({
        "idAlumno": ['', Validators.required] //sera utilizado el registrado anteriormente
      }),
      "grupo": fBuilder.group({
        "idGrupo": ['', Validators.required]
      }),

      detallePago: fBuilder.group({
        "idDetallePago": [''],
        "numeroRecibo": ['', Validators.required],
        "monto": ['', Validators.required],
        "fechaRecibo": ['', Validators.required],
        tipoPago: fBuilder.group({
          "idTipoPago": ['', Validators.required]
        }),

        pago: fBuilder.group({
          "idPago": [''],
          alumno: fBuilder.group({
            "idAlumno": ['',]// al llegar al backend sera utilizado el id de arriba esto porque el jsonidentyinfo no  acepta 2 objetos del mismo tipo con el mismo id
          }),
          administrativo: fBuilder.group({
            "idAdministrativo": ['', Validators.required]
          })
        })

      }),

    })

    this.formPostulacionExamenUbicacion = fBuilder.group({

      alumno: fBuilder.group({
        "idAlumno": ['', Validators.required] //sera utilizado el registrado anteriormente
      }),
      examenUbicacion: fBuilder.group({
        "idExamenUbicacion": ['', Validators.required]
      }),
      detallePago: fBuilder.group({
        "idDetallePago": [''],
        "numeroRecibo": ['', Validators.required],
        "monto": ['', Validators.required],
        "fechaRecibo": ['', Validators.required],
        tipoPago: fBuilder.group({
          "idTipoPago": ['', Validators.required]
        }),

        pago: fBuilder.group({
          "idPago": [''],
          alumno: fBuilder.group({
            "idAlumno": ['',]// al llegar al backend sera utilizado el id de arriba esto porque el jsonidentyinfo no  acepta 2 objetos del mismo tipo con el mismo id
          }),
          administrativo: fBuilder.group({
            "idAdministrativo": ['', Validators.required]
          })
        })

      }),


    })

    this.formPostulanteMatricula = fBuilder.group({
      "idAlumno": [''],
      "tipoAlumno": ['univparticular'],//este valor no representa nada, el tipo de alumno sera asignado en el backend  de acuerdo a su universidad o colegio
      "codigoAlumno": [''],

      "persona": fBuilder.group({
        "idPersona": [''],
        "nombre": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
        "apaterno": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
        "amaterno": ['', [Validators.required, Validators.pattern("^([^0-9]*)$"), RestClient.noWhitespaceValidator]],
        "dni": ['', [Validators.required, Validators.pattern("^[0-9]+$"), Validators.minLength(8), Validators.maxLength(8)], asyncValidatorDNI.createValidatorRegistrar(this.personaService)],
        "fechaNacimimento": ['', Validators.required],
        "sexo": ['', Validators.required],


        "procedencia": ['', [Validators.required, RestClient.noWhitespaceValidator]],
        "telefono": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(7), Validators.maxLength(7),]],
        "celular": ['', [Validators.pattern("^[0-9]+$"), Validators.minLength(9), Validators.maxLength(9),]],
        "gradoInstruccion": ['universidad', Validators.required],
        "correoElectronico": ['', [Validators.email]],
        "foto": [''],
        "distrito": fBuilder.group({
          "idDistrito": ['', Validators.required],
          "provincia": fBuilder.group({
            "idProvincia": [''],
            "nombre": [''],
            "departamento": fBuilder.group({
              "idDepartamento": [''],
              "nombre": ['']
            })
          })

        }),
      }),

      alumnoUniversitario: fBuilder.group({
        "idAlumnoUniversitario": [],
        "ciclo": ['', [Validators.required, Validators.pattern("^[0-9]+$")]],
        "planDeEstudios": ['', [Validators.required, Validators.pattern("^[0-9]+$")]],
        "condicion": ['', Validators.required],
        "gradoInstruccionActivo": true,
        "escuela": fBuilder.group({
          "idEscuela": ['', Validators.required],
          "nombre": [''],
          "facultad": fBuilder.group({
            "idFacultad": ['', Validators.required],
            "nombre": [''],
            "universidad": fBuilder.group({
              "idUniversidad": ['', Validators.required],
              "nombre": [''],
              "siglas": ['']
            })
          })
        }),
      }),


      alumnoColegio: fBuilder.group({
        "idAlumnoColegio": [''],
        "grado": ['', [Validators.required]],
        "nivel": ['', [Validators.required]],
        "gradoInstruccionActivo": true,

        "colegio": fBuilder.group({
          "idColegio": ['', Validators.required]
        }),
      }),


      "usuario": fBuilder.group({
        "idUsuario": [''],
        "username": ['', [Validators.minLength(6), Validators.pattern("^[a-zA-Z0-9]+$")]],
        "password": ['', [Validators.minLength(8)]],
        "tipoUsuario": ['alumno']
      })
    })

  }



  ngOnInit() {
    this.listarTiposDePagoExamenDeUbicacion();

    this.formPostulacionExamenUbicacion.get('detallePago').get('pago').get('administrativo').get('idAdministrativo').setValue(this.currentUser.administrativo.idAdministrativo);

    this.formMatriculaGrupo.get('detallePago').get('pago').get('administrativo').get('idAdministrativo').setValue(this.currentUser.administrativo.idAdministrativo);

    this.formPostulanteMatricula.get("persona").statusChanges.subscribe((data: any) => {
      if (this.formPostulanteMatricula.get('persona').valid === true || this.formPostulanteMatricula.get('persona').disabled === true) {
        this.formpersonaValido = true;
      }
    })



  }

  onPagoMatriculaSelected(booleanValue) {
    console.log(booleanValue);
  }

  onDniPersonaExist(al: any) {
    Object.keys(al).forEach(function (key) {
      if (al[key] === null) {
        al[key] = '';
      }
    })
    // this.formPostulanteMatricula.patchValue(al);
    if (al && al.idAlumno) {
      this.formPostulanteMatricula.get('idAlumno').setValue(al.idAlumno);
      this.formPostulanteMatricula.get('tipoAlumno').setValue(al.tipoAlumno);
      this.formPostulanteMatricula.get('alumnoColegio').disable();
      this.formPostulanteMatricula.get('alumnoUniversitario').disable();


      this.formPostulacionExamenUbicacion.get('alumno').get('idAlumno').setValue(al.idAlumno)
      this.formMatriculaGrupo.get('alumno').get('idAlumno').setValue(al.idAlumno)
      this.personaCargadaPorValidador = true;
      this.alumnoRegistrado = true;
      this.alumnoRegistrado = al;
    }

  }


  onGupoSelected($event) {

    if ($event) {
      this.formMatriculaGrupo.get('grupo').get('idGrupo').setValue($event);
    }
    else {
      this.formMatriculaGrupo.get('grupo').get('idGrupo').setValue('');
    }



  }


//@Carrillo: Sirve para escuchar cambios del componente Modalidad que se encuentra en select-grupo.component, con el cual se limpiaran los tipos de pago  cuando se cambien los valores
  onModalidadChange(modalidad)
  {


        //se limpian los campos de tipo pago carnet
    this.tiposPagoCarnet=[];
    this.formDetPagoCarne.get('tipoPago').get('idTipoPago').setValue('');
    //se limpian los campos de pago matricula
    this.tipospagoMatricula=[];
    this.formMatriculaGrupo.get('detallePago').get('tipoPago').get('idTipoPago').setValue('')
  }
//@Carrillo: Sirve para escuchar cambios del componente Idioma que se encuentra en select-grupo.component, con el cual se limpiaran los tipos de pago  cuando se cambien los valores
  onIdiomaChange(idioma)
  {
    //se limpian los campos de tipo pago carnet
    this.tiposPagoCarnet=[];
    this.formDetPagoCarne.get('tipoPago').get('idTipoPago').setValue('');
    //se limpian los campos de pago matricula
    this.tipospagoMatricula=[];
    this.formMatriculaGrupo.get('detallePago').get('tipoPago').get('idTipoPago').setValue('')


  }

  onNivelSelected($event) {



    this.tipospagoMatricula = [];
    this.tipospagoMatricula=[];
    if($event!='')
    {
      if (this.alumnoRegistrado && this.alumnoRegistrado.tipoAlumno != '') {
        let parameters = $event;

        this.tipoPagoService.getByConceptopTipoPagoMATRICULAndCategoriaTipoPagoAndModalidadAndIdiomaAndNivelAndModalidad(parameters, this.alumnoRegistrado.tipoAlumno).subscribe((data: any) => {

          this.tipospagoMatricula = data.content;
        })
        this.tipoPagoService.getPagosEnGeneralByConcepto('carnet').subscribe((data: any) => {
          this.tiposPagoCarnet = data.content;
        })


      }
      else {
        Swal.fire('EROOR', 'Debe registrar un alumno primero', 'error');
      }
    }
    else{
      this.tiposPagoCarnet=[];
      this.formDetPagoCarne.get('tipoPago').get('idTipoPago').setValue('');
      //se limpian los campos de pago matricula
      this.tipospagoMatricula=[];
      this.formMatriculaGrupo.get('detallePago').get('tipoPago').get('idTipoPago').setValue('')
    }


  }



  seleccionTipoEstudiante($event: MatRadioChange) {

    if ($event.value != null) {
      this.tipoAlumno = $event.value

      switch (this.tipoAlumno) {
        case "colegio":
          this.formPostulanteMatricula.get('alumnoUniversitario').disable();
          this.formPostulanteMatricula.get('alumnoColegio').enable();
          // this.formPostulanteMatricula.get('tipoAlumno').setValue('colegio')
          break;

        case "universidad":
          this.formPostulanteMatricula.get('alumnoColegio').disable();
          this.formPostulanteMatricula.get('alumnoUniversitario').enable();
          // this.formPostulanteMatricula.get('tipoAlumno').setValue('universidad')
          break;
      }


    }
  }


  seleccionTipoOperacion($event: MatRadioChange) {

    if ($event.value != null) {
      this.tipoOperacion = $event.value
    }

  }


  goForward(stepper: MatStepper) {

    //elimina del formulario los tipos de alumno que no se eligieron para  que  el json quede en el formato debido
    switch (this.tipoAlumno) {
      case "colegio":
        this.formPostulanteMatricula.removeControl('alumnoUniversitario');
        break;

      case "universidad":
        this.formPostulanteMatricula.removeControl('alumnoColegio');
        break;
    }


    this.alumnoService.registrarAlumno(this.formPostulanteMatricula.getRawValue()).subscribe((data: any) => {


      if (!data.error) {
        Swal.fire('Exito', 'Se registro al alumno ' + data.content.persona.nombre + ' ' + data.content.persona.apaterno + ' ' + data.content.persona.amaterno + ' existosamente. Por favor a continue con el formulario', 'success')

        this.alumnoRegistrado = data.content;
        this.formPostulacionExamenUbicacion.get('alumno').get('idAlumno').setValue(data.content.idAlumno)
        this.formMatriculaGrupo.get('alumno').get('idAlumno').setValue(data.content.idAlumno)
        // Carrillo-> lo quite porque al enviarlo a spring  no puede deserializar porque el JsonIdentityInfo detecta 2 veces el mismo id en un mismo objeto
        //en cambio lo seteare ya en el spring utilizando el idalumno de arriba asi solo lo recibira 1 vez
        //*ver el metodo --save de --PostulacionServiceImpl-- en el proyecto Spring
        // this.formPostulacionExamenUbicacion.get('detallePago').get('pago').get('alumno').get('idAlumno').setValue(data.content.idAlumno)
        stepper.selected.completed = true;

        stepper.next();

        this.cargando = false;

      }
      else {
        Swal.fire('Error', 'Ocurrio un error al intentar registrar el alumno: ' + data.mensaje, 'error')
        this.cargando = false;
      }
    }, error => {
      Swal.fire('Error', 'Ocurrio un error al intentar registrar el alumno: ' + error.error.mensaje, 'error')
      this.cargando = false;
    })


  }


  listarTiposDePagoExamenDeUbicacion() {
    return this.tipoPagoService.listarTiposDePagoExamenUbicacion().subscribe((data: any) => {
      this.tipospagoExuBicacion = data.content;
    })
  }



  registrarPostulacion(formPostulacionValue) {
    this.cargando = true;
    this.postulacionService.registrarPostulacion(formPostulacionValue).subscribe((data: any) => {
      if (data.error == false) {
        this.cargando = false;
        Swal.fire('EXITO!', 'La postulacion fue registrada exitosamente', 'success')
      } else {
        this.cargando = false;
        Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
      }
    }, error => {
      this.cargando = false;
      Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' + error.error.mensaje, 'error')
    })
  }


  registrarMatriculaGrupo(formMatriculaGrupovalue) {
    this.cargando = true;


    this.matriculaService.registrarMatricula(formMatriculaGrupovalue,this.formDetPagoCarne.value).subscribe((data: any) => {
      if (data.error == false) {
        this.cargando = false;
        Swal.fire('EXITO!', 'La matricula fue registrada exitosamente', 'success')
      } else {
        this.cargando = false;

        Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mansaje, 'error')
      }
    }, error => {

      Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' + error.error.mensaje, 'error')
    })
  }



  ngAfterViewInit(): void {
    //Esto sirve para que se copie el valor del campo dni en username y password de usuario
    this.formPostulanteMatricula.get('persona').get('dni').valueChanges.subscribe(val => {
      // console.log(this.alumnoUniversidad);
      this.formPostulanteMatricula.get('usuario').get('username').setValue(val)
      this.formPostulanteMatricula.get('usuario').get('password').setValue(val)
    });
  }



  onPagoCarnentSelected()
  {
    this.pagarCarne=!this.pagarCarne;
    this.formDetPagoCarne.reset();
    //@Carrillo: se asigna como '' porque luego del reset el numerorecibo pasa como null y genera problema con la validacion en el backend en el metodo equals
    this.formDetPagoCarne.get('numeroRecibo').setValue('')
  }



  // habilitarFormPersona()
  // {
  //   if(this.formPostulanteMatricula.get('persona').disabled===true)
  //   {
  //     this.personaCargadaPorValidador=true;
  //     this.formPostulanteMatricula.get('persona').get('dni').clearAsyncValidators();
  //     this.formPostulanteMatricula.get('persona').enable();
  //   }

  // }

}
