import { NgModule } from "@angular/core";
import { CrearMatriculaComponent } from './crear-matricula.component';
import { Route } from '@angular/compiler/src/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { GrupoModule } from '../../grupomodule/grupo.module';
import { PagoModule } from '../../pagomodule/pago.module';
import { MatStepperModule, MatRadioModule, MatCardModule, MatButtonModule } from '@angular/material';
import { ExamenubicacionModule } from '../../examenubicacionmodule/examenubicacion.module';
import { AlumnoModule } from '../../alumnomodule/alumno.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsgeneralModule } from '../../componentsgeneral/componentsgeneral.module';

const MATRICULA_ROUTES:Routes=[

 {
     path:'matricula',
     children:
     [
        {path:'registrar', component:CrearMatriculaComponent}
     ]
 }

];


@NgModule({
    declarations: [CrearMatriculaComponent],
    exports: [],
    imports: 
    [
        CommonModule,
        MatStepperModule,
        MatRadioModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatButtonModule,
        ComponentsgeneralModule,   
        ExamenubicacionModule,
        AlumnoModule,
        GrupoModule,
        PagoModule,

        RouterModule.forChild(MATRICULA_ROUTES)
    ]
})

export class MatriculaModule{}