import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsgeneralModule } from './componentsgeneral/componentsgeneral.module';
import { PagoModule } from './pagomodule/pago.module';
import { ExamenubicacionModule } from './examenubicacionmodule/examenubicacion.module';
import { GrupoModule } from './grupomodule/grupo.module';
import { MatStepperModule, MatButtonModule, MatProgressSpinnerModule, MatRadioModule } from '@angular/material';
import { AlumnoModule } from './alumnomodule/alumno.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PAGES_ROUTES } from './pages.routing';
import { SharedModule } from '../shared/shared.module';
import { DocenteModule } from './docentemodule/docente.module';
import { AdministrativoModule } from './administrativomodule/administrativo.module';
import { MatriculaModule } from './matriculamodule/crear-matricula/matricula.module';
import { UserModule } from './usermodule/user.module';


@NgModule({
    declarations: [
        
        DashboardComponent,
    ],

    imports:[
        CommonModule,
        ComponentsgeneralModule,        
        MatStepperModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatRadioModule,

        
        AlumnoModule,
        PagoModule,
        ExamenubicacionModule,
        UserModule,
        DocenteModule,
        AdministrativoModule,
        GrupoModule,
        SharedModule,
        UserModule,
        MatriculaModule,
        PagoModule,
        PAGES_ROUTES
    ]
})

export class PagesModule { }