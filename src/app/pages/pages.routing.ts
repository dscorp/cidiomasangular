import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from './dashboard/dashboard.component';
import { CrearMatriculaComponent } from './matriculamodule/crear-matricula/crear-matricula.component';

const pagesRoutes: Routes = [

    { path: '', component: DashboardComponent },
    { path: 'matricula/registro', component: CrearMatriculaComponent }
];


export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
