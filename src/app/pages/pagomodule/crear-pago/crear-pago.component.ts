import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { TipopagoService } from '../tipopago/tipopago.service';
import { PagoService } from 'src/app/services/pago.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
import { DetallePago } from 'src/app/model/models';

@Component({
  selector: 'app-pago',
  templateUrl: './crear-pago.component.html',
  styleUrls: ['./crear-pago.component.css']
})
export class CrearPagoComponent implements OnInit {

  tiposPago: any[] = []
  displayedColumns: string[] = ['numeroRecibo', 'monto', 'fechaRecibo', 'conceptoPago'];
  formPago: FormGroup;

  formDetPago: FormGroup;

  detallePago: DetallePago = new DetallePago();


  constructor(
    private fbuilder: FormBuilder,
    private tipoPagoService: TipopagoService,
    private pagoService: PagoService,
    private authService: AuthService

  ) {

    this.formPago = fbuilder.group({
      "idPago": [''],
      "fecha": [new Date()],
      "detalles": new FormArray([]),

      "alumno": fbuilder.group({
        "idAlumno": ['', Validators.required]
      }),
      "administrativo": fbuilder.group({
        "idAdministrativo": [authService.currentUserValue.administrativo.idAdministrativo, Validators.required]
      })
    })


    this.formDetPago = this.fbuilder.group
      ({
        "idDetallePago": [''],
        "numeroRecibo": ['', Validators.required],
        "monto": ['', Validators.required],
        "fechaRecibo": ['', Validators.required],
        "tipoPago": this.fbuilder.group
          ({
            "idTipoPago": ['', Validators.required],
            "conceptoPago": this.fbuilder.group({
              "idConceptoPago": [''],
              "conceptoPago": ['']
            })
          }, Validators.required),
      })



  }



  cargarTipoPagoGenerales() {
    this.tipoPagoService.getGenerales().subscribe((data: any) => {
      this.tiposPago = data.content;
    })
  }




  agregarDetallePago() {
    let a = this.formDetPago.value;

    let paraAgregar: FormGroup = this.fbuilder.group({

      "idDetallePago": '',
      "numeroRecibo": a.numeroRecibo,
      "monto": a.monto,
      "fechaRecibo": a.fechaRecibo,
      "tipoPago": this.fbuilder.group({
        "idTipoPago": a.tipoPago.idTipoPago,
        "conceptoPago": this.fbuilder.group({
          "idConceptoPago": a.tipoPago.conceptoPago.idConceptoPago,
          "conceptoPago": a.tipoPago.conceptoPago.conceptoPago
        })
      }),

    });





    if ((this.formPago.get('detalles') as FormArray).length < 1) {
      (this.formPago.get('detalles') as FormArray).push(paraAgregar)
      this.formDetPago.reset();

    }
    else {
      (this.formPago.get('detalles') as FormArray).controls.forEach(element => {

                  if (element.get('tipoPago').get('conceptoPago').get('conceptoPago').value != paraAgregar.get('tipoPago').get('conceptoPago').get('conceptoPago').value) {

                    (this.formPago.get('detalles') as FormArray).push(paraAgregar)
                    // this.formDetPago.reset();

                  }
                  else {
                    Swal.fire('Advertencia', 'Ya existe un pago con este concepto en la lista', 'info')
                  }

      });
    }


    console.log(this.formPago.get('detalles').value);
  }

  ontpselected(tipoPago) {


    let obj = JSON.parse(tipoPago.target.value);
    if (tipoPago.target.value) {

      this.formDetPago.get('tipoPago').get('idTipoPago').setValue(obj.idTipoPago)
      this.formDetPago.get('tipoPago').get('conceptoPago').get('idConceptoPago').setValue(obj.conceptoPago.idConceptoPago)
      this.formDetPago.get('tipoPago').get('conceptoPago').get('conceptoPago').setValue(obj.conceptoPago.conceptoPago)
    } else {
      this.formDetPago.get('tipoPago').get('idTipoPago').setValue('')
      this.formDetPago.get('tipoPago').get('conceptoPago').get('idConceptoPago').setValue('')
      this.formDetPago.get('tipoPago').get('conceptoPago').get('conceptoPago').setValue('')
    }

  }


  ngOnInit() {
    this.cargarTipoPagoGenerales();
  }


  show() {

    console.log(this.formDetPago);

    console.log(this.formPago.get('detalles').value)
  }




  registrar() {
    this.pagoService.registrar(this.formPago.value).subscribe((data: any) => {

      if (data.error === false) {
        Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
      }
      else {

        Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
      }

    })
  }


  detallesasArraylenght(): FormArray {
    let arr = this.formPago.get('detalles') as FormArray;
    return arr;
  }
}
