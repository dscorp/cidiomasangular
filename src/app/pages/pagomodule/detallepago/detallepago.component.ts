import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';



@Component({
  selector: 'app-detallepago',
  template: `
              <div  [formGroup]="formdetallepago">

                <div class="col-lg-4">
                  <div class="form-group">
                    <label>Fecha De Recibo</label>
                    <input type="date" class="form-control" placeholder="" formControlName="fechaRecibo">
                    <div class="text-danger" *ngIf="formdetallepago.get('fechaRecibo').errors?.required">
                      Debe seleccionar una fecha 
                    </div>
                  </div>
                </div>

                <div class="col-lg-4">
                  <div class="form-group">
                  <label>Codigo De Recibo</label>
                  <input type="text" class="form-control" formControlName="numeroRecibo">
                  <div class="text-danger" *ngIf="formdetallepago.get('numeroRecibo').errors?.required">
                    Debe digitar el codigo del recibo
                  </div>
              </div>
                </div>

                <div class="col-lg-4">
                  <div class="form-group">
                  <label for="">Monto De Recibo</label>
                  <input type="number"  min="0.00" max="10000.00" step="0.1" class="form-control" placeholder="" formControlName="monto">
                  <div class="text-danger" *ngIf="formdetallepago.get('monto').errors?.required">
                    Debe digitar el monto del recibo
                  </div>
                </div>
      </div>
      </div>
          
  `
})
export class DetallepagoComponent implements OnInit {


  @Input()
  formdetallepago: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
