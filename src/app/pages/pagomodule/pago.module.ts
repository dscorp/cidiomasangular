import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetallepagoComponent } from './detallepago/detallepago.component';
import { TipopagoComponent } from './tipopago/tipopago.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatDialogClose, MatDialogModule, MatStepperModule, MatTableModule, MatIcon, MatButtonModule, MatIconModule, MatPaginatorModule, MatCardModule, MatButton } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { CrearPagoComponent } from './crear-pago/crear-pago.component';
import { AlumnoModule } from '../alumnomodule/alumno.module';


const routes: Routes = [
  {
    path: 'pago', children: [
      { path: 'add', component: CrearPagoComponent }

    ]
  }

]


@NgModule({
  declarations: [
    DetallepagoComponent,
    TipopagoComponent,
    CrearPagoComponent,
  ],
  exports: [
    DetallepagoComponent,
    TipopagoComponent
  ],
  imports: [
    //modulos del proyecto
    AlumnoModule,

    //modulos de angular
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatStepperModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatPaginatorModule,
    RouterModule,
    MatCardModule,
    MatButtonModule,
    MatTableModule,
    RouterModule.forChild(routes)
  ]
})
export class PagoModule { }
