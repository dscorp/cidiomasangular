import { Component, OnInit, Input } from '@angular/core';
import { TipopagoService } from './tipopago.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-tipopago',
  template: `

<div [formGroup]="formTipoPago" >

   <select class="form-control" formControlName="idTipoPago">
    <option value="">Seleccione un tipo de pago</option>
    <option *ngFor="let t of tipospago" value="{{t.idTipoPago}}"> S/. {{t.precio}} - TUPA:{{t.tupa.nombre}} - {{t.conceptoPago.conceptoPago}} 
    </option>
  </select>
  <div class=" text-danger" *ngIf="formTipoPago.controls['idTipoPago'].errors?.required">
      Debe seleccionar un tipo de pago
    </div>

</div>
  `
})
export class TipopagoComponent implements OnInit {


  @Input()
  formTipoPago:FormGroup;

  @Input()
  tipospago: any[] = [];

  constructor(
    private tipoPagoservice: TipopagoService
  ) { }

  ngOnInit() {
  }


  // listarTiposPago() {
  //   this.tipoPagoservice.listarTiposPago(1).subscribe((data: any) => {
  //     this.tipospago = data.content;
  //   })
  // }


}
