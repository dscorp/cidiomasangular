import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestClient } from '../../../Util/restClient';

@Injectable({
  providedIn: 'root'
})
export class TipopagoService {
  path = RestClient.URL_SERVER + '/TipoPago'

  constructor(
    private http: HttpClient
  ) { }


  getGenerales()
  {
    let query = this.path+'/general';
    return this.http.get(query);
  }

    getPagosEnGeneralByConcepto(concepto:string)
    {
      let query = this.path+'/general/byconcepto/'+concepto;
      console.log(query);
      return this.http.get(query);
    }


  getByConceptopTipoPagoMATRICULAndCategoriaTipoPagoAndModalidadAndIdiomaAndNivelAndModalidad(parameters: any, tipoALumno: string) {
    let categoriaTipoPago = '';
   
   //aqui se realiza la transofrmacion de acuerdo al tipo de alumno para listar los tipospago pagos que le corresponden
    switch (tipoALumno) {
      case 'univfaustiniano':
        categoriaTipoPago = 'FAUSTINIANO'
        break;
      case 'otrosfastunianos':
        categoriaTipoPago = 'FAUSTINIANO';
        break;
      case 'univparticular':
        categoriaTipoPago = 'PARTICULAR';
        break;
      case 'colegio':
        categoriaTipoPago = 'PARTICULAR';
        break;
      case 'convenio':
        categoriaTipoPago = 'CONVENIO'
    }

    return this.http.get(this.path + '/matricula/' + categoriaTipoPago + '/' + parameters.idModalidad + '/' + parameters.idIdioma + '/' + parameters.idNivel);
  }

  listarTiposDePagoExamenUbicacion() {
    return this.http.get(this.path + '/examenubicacion');
  }


  listarTiposPago(idCiclo) {
    return this.http.get(this.path + idCiclo);
  }

}
