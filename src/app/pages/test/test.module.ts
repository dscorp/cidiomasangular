import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { TestComponent } from './test.component';
import { EllaComponent } from './ella/ella.component';

const routes: Routes = [
  {
    path: ' test', children :[
      { path: 'ahiezer' , component: TestComponent },
      { path: 'ella' , component: EllaComponent}
  ]
}

]

@NgModule({
  declarations: [
    TestComponent,
    EllaComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})

export class TestModule { }
