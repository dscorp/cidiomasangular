import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioService } from 'src/app/components/user/usuario.service';

@Component({
  selector: 'app-configuracion-usuario',
  templateUrl: './configuracion-usuario.component.html',
  styleUrls: ['./configuracion-usuario.component.css']
})
export class ConfiguracionUsuarioComponent implements OnInit {

  public configuracionForm;
  public cargando: boolean = false;

  constructor(
    private fBuilder : FormBuilder,
    private usuarioService : UsuarioService,
    private _router: Router,
    private auth: AuthService
  ) {

    this.configuracionForm = fBuilder.group({
      "idUsuario": [''],
      "nowpassword": ['' , Validators.required],
      "newpassword": ['' , Validators.required],
      "repeat_newpassword": ['' ,Validators.required],
    });

  }

  ngOnInit() {
  }

  form(){
    return this.configuracionForm;
  }

  request: Observable<any>;
  submit(formValue) {
    this.cargando = true;

    // stop here if form is invalid
    if (this.configuracionForm.invalid) {
        return;
    }

    this.configuracionForm.get("idUsuario").setValue(this.auth.currentUserValue.idUsuario);
    
    console.log(this.configuracionForm.value);

    this.request = this.usuarioService.changePass(this.configuracionForm.value);

    this.request.subscribe((data: any) => {
      if (data.error===false) {
        Swal.fire('Exito', 'Operacion realizada Exitosamente', 'success')
        this._router.navigate([''])
        this.cargando = false;
      }
      else {
        Swal.fire(data)
        Swal.fire('Error', 'Ocurrio un error al intentar registrar la tarea: ' + data.mensaje, 'error')
        this.cargando=false;
      }

    }, error => {
      this.cargando = false;
      console.log(error)
      Swal.fire('Error', 'Ocurrio un error al intentar realizar la tarea: ' +error.error.mensaje, 'error')
    }) 


  }

}
