import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, TooltipPosition, MatDialog } from '@angular/material';
import { CambioClaveDialog } from 'src/app/components/user/cambio-clave-dialog/cambio-clave-dialog.component';
import { UsuarioService } from 'src/app/components/user/usuario.service';


@Component({
  selector: 'app-listar-usuario-administrativo',
  templateUrl: './listar-usuario-administrativo.component.html',
  styleUrls: ['./listar-usuario-administrativo.component.css']
})
export class ListarUsuarioAdministrativoComponent implements OnInit {

usuario:any[]=[];
displayColumns = ['nombre', 'paterno', 'materno',  'dni', 'usuario', 'tipoAdministrativo', 'actions']
  dataSource: MatTableDataSource<any>;
 
  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;

  constructor(
    private usuarioService: UsuarioService ,
    public dialog:MatDialog,
    )
     {
     this.listarUsuarioAdministrativo();
     }


  listarUsuarioAdministrativo(){
    this.usuarioService.listarUsuarioAdministartivo().subscribe((data:any)=>{
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log(data)
      console.log(this.dataSource)
    });
  }

  ngOnInit() {

}
applyFilter(filterValue: string) {
  filterValue = filterValue.trim(); // Remove whitespace
  filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
  this.dataSource.filter = filterValue;
}

openDialog(idUsuarioRecibido,nombreRecibido): void {
  const dialogRef = this.dialog.open(CambioClaveDialog, {
    width: '500px',
    data: {id: idUsuarioRecibido, nombre: nombreRecibido}
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
    // this.animal = result;
  });
}

}
