import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator, MatDialog } from '@angular/material';
import { UsuarioService } from 'src/app/components/user/usuario.service';
import { CambioClaveDialog } from 'src/app/components/user/cambio-clave-dialog/cambio-clave-dialog.component';



@Component({
  selector: 'app-listar-usuario-docente',
  templateUrl: './listar-usuario-docente.component.html',
  styleUrls: ['./listar-usuario-docente.component.css']
})
export class ListarUsuarioDocenteComponent implements OnInit {

  usuario:any[]=[];
  displayColumns = ['nombre', 'paterno', 'materno',  'dni','usuario','actions']
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator,{static: false}) paginator: MatPaginator;
  @ViewChild(MatSort,{static: false}) sort: MatSort;
  
  constructor(private usuarioService: UsuarioService
    , public dialog:MatDialog
    ) {
      this.listarUsuarioDocente();
      
   }

   listarUsuarioDocente(){
     this.usuarioService.listarUsuarioDocente().subscribe((data:any) =>{
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.paginator  = this.paginator;
      this.dataSource.sort = this.sort;
      console.log(data)
      console.log(this.dataSource)
     });
   }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openDialog(idUsuarioRecibido,nombreRecibido): void {
    const dialogRef = this.dialog.open(CambioClaveDialog, {
      width: '500px',
      data: {id: idUsuarioRecibido, nombre: nombreRecibido}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });
  }

}
