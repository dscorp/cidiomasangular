import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule, MatIconModule, MatSelectModule, MatTableModule, MatPaginatorModule, MatInputModule, MatButtonModule, MatCardModule } from '@angular/material';
import { Routes, RouterModule } from '@angular/router';


import { ListarUsuarioComponent } from './listar-usuario-alumno/listar-usuario-alumno.component';
import { ListarUsuarioDocenteComponent } from './listar-usuario-docente/listar-usuario-docente.component';
import { ConfiguracionUsuarioComponent } from './configuracion-usuario/configuracion-usuario.component';
import { ListarUsuarioAdministrativoComponent } from './listar-usuario-administrativo/listar-usuario-administrativo.component';
import { ListarUsuariosTodos } from './listar-usuarios-todos/listar-usuarios-todos.component';
import { CambioClaveDialog } from 'src/app/components/user/cambio-clave-dialog/cambio-clave-dialog.component';

const routes: Routes = [

  {
    path: 'usuario',
    children:
      [
        { path: '', component: ListarUsuariosTodos },
        { path: 'alumno', component: ListarUsuarioComponent },
        { path: 'docente', component: ListarUsuarioDocenteComponent },
        { path: 'administrativo' , component : ListarUsuarioAdministrativoComponent},
        { path: 'configuracion' , component: ConfiguracionUsuarioComponent }
      ]
  }

]

@NgModule({
  declarations: [
 
    ListarUsuarioComponent,
    ListarUsuarioDocenteComponent,
    ListarUsuarioAdministrativoComponent,
    ConfiguracionUsuarioComponent,
    ListarUsuariosTodos,
    CambioClaveDialog
  ],

  exports: [

  ],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatCardModule
  ],

  entryComponents: [
    CambioClaveDialog
  ]
})
export class UserModule { }
