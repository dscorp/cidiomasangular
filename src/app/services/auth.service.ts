import { Injectable } from '@angular/core';
import { Usuario } from 'src/app/model/models';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { RestClient } from 'src/app/Util/restClient';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<Usuario>;
  public currentUser: Observable<Usuario>;

  constructor(
    private http: HttpClient,
    private _router: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<Usuario>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
   }

   public get currentUserValue(): Usuario {
    return this.currentUserSubject.value;
}


login(username: string, password: string) {
  let idUsuario:0;
  let usuario  = new Usuario();
  usuario.idUsuario=0;
  usuario.username=username;
  usuario.password=password;
    return this.http.post<any>(RestClient.URL_SERVER+'/usuario/login', usuario)
        .pipe(map(response => {
            // login successful if there's a jwt token in the response
            if (response.usuario) {
                console.log("Usuario existe proceder");
                console.log(response.usuario);
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(response.usuario));
                console.log(response.usuario);
                this.currentUserSubject.next(response.usuario);
                return response.usuario;
            }else{
              throw new Error(response.message);
            }
        }));
}

logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this._router.navigate(['login'])
}

}
