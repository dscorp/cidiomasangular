import { Component, OnInit, Injectable, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Observable, of, Subscription, Subject } from 'rxjs';
import { map, startWith, catchError } from 'rxjs/operators';
import { AlumnoService } from 'src/app/components/alumno/alumno.service';
import { switchMap, debounceTime, tap, finalize } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements OnInit {


  @Output()

  outAlumno = new EventEmitter<any>();

  public githubAutoComplete$: Observable<any> = null;
  public autoCompleteControl = new FormControl();

  constructor
    (
      private alumnoService: AlumnoService,

  ) {

  }


  ngOnInit() {

    this.githubAutoComplete$ = this.autoCompleteControl.valueChanges.pipe(
      startWith(''),
      // delay emits
      debounceTime(300),
      // use switch map so as to cancel previous subscribed events, before creating new once
      switchMap(value => {
        if (value !== '') {
          // lookup from github
          return this.lookup(value);
        } else {
          // if no value is pressent, return null
          return of(null);
        }
      })
    );
  }

  lookup(value: string): Observable<any> {
    return this.alumnoService.searchByNombreOrApeedioOrDNI(value.toLowerCase()).pipe(
      // map the item property of the github results as our return object
      map(results => {

        return results.content;
      }),
      // catch errors
      catchError(_ => {
        return of(null);
      })
    );
  }




  displayFn(alumno) {
    this.outAlumno.emit(alumno)
  }


}



