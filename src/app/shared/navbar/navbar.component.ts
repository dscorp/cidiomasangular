import { Component, OnInit, Output, Input } from '@angular/core';
import {LoginService} from 'src/app/components/login/login.service'
import {Router} from '@angular/router'
import { AuthService } from 'src/app/services/auth.service';
import { Usuario } from 'src/app/model/models';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public appname:string = "CENTRO DE IDIOMAS";
  public user: Usuario;

  constructor(
    private router: Router,
    private auth: AuthService
  ) { 
    this.user = auth.currentUserValue;
  }

  ngOnInit() {

  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }
  
}
