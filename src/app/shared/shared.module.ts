import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { NavbarComponent } from './navbar/navbar.component';



@NgModule({
  declarations: [
    SidebarComponent,
    NavbarComponent,
    NopagefoundComponent

  ],
  imports: [
    CommonModule,
    RouterModule,

  ],
  exports:[
    SidebarComponent,
    NavbarComponent,
    NopagefoundComponent
  ]
})
export class SharedModule { }
